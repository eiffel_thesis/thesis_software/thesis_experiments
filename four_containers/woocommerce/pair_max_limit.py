#! /usr/bin/env python3
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
import sys
import docker
import os
import time
import common


"""TODO
"""

CHARS = ['A', 'Y', 'B', 'Z']

def main():
	containers = []
	outs = []
	stats = []

	if len(sys.argv) < 3:
		sys.exit("Usage: %s max_limit_A max_limit_B" % sys.argv[0])

	# In this experiment we will only run woocommerce containers inside the
	# virtual machine.
	# Indeed, gatling containers must not be reclaimed since they represent client
	# request.
	# So this script will be run from the host and create containers in both the
	# host and the guest.
	# To do this we need to create two docker clients.
	gatling_client = docker.from_env()
	woocommerce_client = docker.DockerClient(base_url = 'tcp://127.0.0.1:12375')

	# Launch containers as detached.
	# We first launch first pairs with according port.
	containers.append(woocommerce_client.containers.run('woocommerce', command = "8080", ports = {"80/tcp": 8080}, detach = True, mem_limit = '3G'))
	# Gatling containers will use the host network to communicate with containers
	# inside the guest.
	containers.append(gatling_client.containers.run('gatling', command = "8080", detach = True, mem_limit = '3G', network_mode = 'host'))

	# Then second one.
	containers.append(woocommerce_client.containers.run('woocommerce', command = "8081", ports = {"80/tcp": 8081}, detach = True, mem_limit = '3G'))
	containers.append(gatling_client.containers.run('gatling', command = "8081", detach = True, mem_limit = '3G', network_mode = 'host'))

	# Wait 240 seconds so the containers are ready. Indeed, it can be slow because
	# mysql is long to boot.
	time.sleep(240)

	# Open /proc/sys/vm/drop_caches to be able to drop linux page cache.
	drop_cache = open('/proc/sys/vm/drop_caches', 'wb', buffering = 0)

	# Every files created in this script will be stored in this directory.
	# Due to the concatenation of the date it should be unique.
	directory = '%s/%s-%s' % (os.path.expanduser('~'), sys.argv[0], time.ctime().replace(' ', '_'))

	os.mkdir(directory)

	# This file will store the content of /proc/stat.
	# We want this obtain the time passed in kernel.
	proc_stat_out = open('%s/%s_proc_stat' % (directory, sys.argv[0]), 'w')

	try:
		common.prepare(containers, CHARS, directory, outs, stats, drop_cache)

		# Update containers' limits.
		# First the woocommerce containers which will have a max limit.
		containers[0].update(mem_limit = sys.argv[1], memswap_limit = sys.argv[1], cpuset_cpus = common.CONTAINERS_CPUS_SETS[0])
		containers[2].update(mem_limit = sys.argv[2], memswap_limit = sys.argv[2], cpuset_cpus = common.CONTAINERS_CPUS_SETS[1])

		# Then gatling containers which have no max limit.
		containers[1].update(mem_limit = '3G', memswap_limit = '3G', cpuset_cpus = common.CONTAINERS_CPUS_SETS[2])
		containers[3].update(mem_limit = '3G', memswap_limit = '3G', cpuset_cpus = common.CONTAINERS_CPUS_SETS[3])

		# Run the experiment.
		common.run(directory, containers, common.RUN_COMMANDS, outs, stats, drop_cache, proc_stat_out)
	finally:
		# If an exception occurs we will not treat it particularly because the whole
		# run needs to be run again.
		# So just printing the callgraph is OK.
		# But either an exception occurs or not we need to clean everything.
		common.clean(containers, outs, stats, drop_cache, proc_stat_out)

if __name__ == "__main__":
	main()
#! /usr/bin/env bash
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
# SPDX-License-Identifier: MPL-2.0


# Change this to your gatling bin path.
GATLING_BIN=/home/francis/These/container/gatling-charts-highcharts-bundle-3.2.1/bin/gatling.sh

for i in alone*/*results.tar; do
	dir=$(dirname $i)
	container_name=$(basename $i .results.tar)

	cd $dir

	tar xvf $(basename $i)

	mv results $container_name

	if [ -f $container_name.report ]; then
		# Empty file if it already exists.
		echo '' > $container_name.report
	fi

	# Generate report and append them to report file.
	for j in $container_name/*; do
		$GATLING_BIN -rf $(dirname $j) -ro $(basename $j) >> $container_name.report
	done

	cd ..
done

for i in alone*/*.report pair*/*.report; do
	perl parse_report.pl $i
done

for i in alone*/*.stats pair*/*.stats; do
	perl parse_stats.pl $i
done

# for i in pair*/*_proc_stat; do
# 	perl parse_proc_stat.pl $i
# done

# for i in pair_default pair_soft_limit pair_mechanism; do
# 	cd $i
#
# 	# The following perl script need to parse the ouput of trace-cmd report so we
# 	# firstly need to create it.
# 	for j in *.dat; do
# 		echo "$i/$j"
# 		trace-cmd report -i $j > $j.trace
# 	done
#
# 	cd ..
#
# 	perl parse_trace.pl $i/*.trace
#
# 	# Parse the 5th files because it is the "middle" of the files.
# 	perl parse_trace_time.pl $i/*_5*.trace
# done
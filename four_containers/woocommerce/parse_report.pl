#! /usr/bin/env perl
# SPDX-License-Identifier: MPL-2.0
use strict;
use warnings;


my $fd_in;
my $fd_out;

my $report;

my $run;
my $str;

$report = shift or die "Usage: $0 file.report";

open $fd_in, '<', $report or die "Problem while opening ${report}: $!";
open $fd_out, '>', "${report}.csv" or die "Problem while opening ${report}.csv: $!";

print ${fd_out} "run;total_request;ko_requests;min_response_time;max_response_time;mean_response_time;std_deviation;99th_response_time;mean_requests_per_second\n";

$run = 0;
$str = "${run}";

while(my $line = <$fd_in>){
	if($line =~ /request count\s+(\d+).*KO=(\d+)/){
		$str .= ";$1;$2";
	}

	if($line =~ /min response time\s+(\d+)/
		|| $line =~ /max response time\s+(\d+)/
		|| $line =~ /mean response time\s+(\d+)/
		|| $line =~ /std deviation\s+(\d+)/
		|| $line =~ /response time 99th percentile\s+(\d+)/
		|| $line =~ m-mean requests/sec\s+(\d+(?:\.\d+)?)-
	){
		$str .= ";$1";
	}

	# I know this is a redo from above but I need to deal with this match
	# particularly.
	if($line =~ m-mean requests/sec\s+(\d+(?:\.\d+)?)-){
		# Indeed this is the last information of a report so parsing it means to
		# pass to next run since we pass to next report.
		$run++;

		print ${fd_out} "${str}\n";

		$str = "${run}";
	}
}

close $fd_in or warn "Problem while closing ${report}: $!";
close $fd_out or warn "Problem while closing ${report}.csv: $!";
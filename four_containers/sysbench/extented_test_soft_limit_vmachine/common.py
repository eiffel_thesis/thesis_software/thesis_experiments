#! /usr/bin/env python3
import re
import time
import json
import threading
import docker
import subprocess
import sys
import pdb

# Runs last 1200 for each container.
DURATIONS = [1200, 1200, 1200, 1200]

# Give one core to each container.
CONTAINERS_CPUS_SETS = ['0,2', '1,3', '4,6', '5,7']

# Containers' DB will be 2G.
# Each will execute with one thread since they have one core.
_SYSBENCH_PREPARE_COMMAND = 'sysbench --threads=2 /usr/local/share/sysbench/oltp_read_only.lua --table_size=10000000 prepare'

# TODO
SYSBENCH_RUN_COMMANDS = ['sysbench --threads=2 --time=1200 --rate=218 --scheduled-rate=218,0,297,0,248,0 --scheduled-time=120,600,840,960,1080,1200 --report-interval=1 --report_checkpoints=120,240,360,480,600,720,840,960,1080,1200 /usr/local/share/sysbench/oltp_read_only.lua --table_size=10000000 run', 'sysbench --threads=2 --time=1200 --rate=192 --scheduled-rate=192,0,210,0,242 --scheduled-time=240,840,960,1080,1200 --report-interval=1 --report_checkpoints=120,240,360,480,600,720,840,960,1080,1200 /usr/local/share/sysbench/oltp_read_only.lua --table_size=10000000 run', 'sysbench --threads=2 --time=1200 --rate=236 --scheduled-rate=236,0,197,0,233 --scheduled-time=360,720,960,1080,1200 --report-interval=1 --report_checkpoints=120,240,360,480,600,720,840,960,1080,1200 /usr/local/share/sysbench/oltp_read_only.lua --table_size=10000000 run', 'sysbench --threads=2 --time=1200 --rate=0 --scheduled-rate=0,110,0 --scheduled-time=480,1080,1200 --report-interval=1 --report_checkpoints=120,240,360,480,600,720,840,960,1080,1200 /usr/local/share/sysbench/oltp_read_only.lua --table_size=10000000 run']

# TODO
SYSBENCH_RUN_COMMANDS_MECHANISM = ["bash -c 'sysbench --threads=2 --time=1200 --rate=218 --scheduled-rate=218,0,297,0,248,0 --scheduled-time=120,600,840,960,1080,1200 --report-interval=1 --report_checkpoints=120,240,360,480,600,720,840,960,1080,1200 /usr/local/share/sysbench/oltp_read_only.lua --table_size=10000000 run | perl probe.pl 2000'", "bash -c 'sysbench --threads=2 --time=1200 --rate=192 --scheduled-rate=192,0,210,0,242 --scheduled-time=240,840,960,1080,1200 --report-interval=1 --report_checkpoints=120,240,360,480,600,720,840,960,1080,1200 /usr/local/share/sysbench/oltp_read_only.lua --table_size=10000000 run | perl probe.pl 2000'", "bash -c 'sysbench --threads=2 --time=1200 --rate=236 --scheduled-rate=236,0,197,0,233 --scheduled-time=360,720,960,1080,1200 --report-interval=1 --report_checkpoints=120,240,360,480,600,720,840,960,1080,1200 /usr/local/share/sysbench/oltp_read_only.lua --table_size=10000000 run | perl probe.pl 2000'", "bash -c 'sysbench --threads=2 --time=1200 --rate=0 --scheduled-rate=0,110,0 --scheduled-time=480,1080,1200 --report-interval=1 --report_checkpoints=120,240,360,480,600,720,840,960,1080,1200 /usr/local/share/sysbench/oltp_read_only.lua --table_size=10000000 run | perl probe.pl 2000'"]

_CPU_INFO = '/proc/cpuinfo'

def monitor_cpu_freq(out, barrier):
	"""This function monitors the cores frequencies every second.
	It writes those information in a csv file.
	:param out: An open in write file object where the frequencies will be
	written.
	:type out: _io.TextIOWrapper
	:param barrier: This function will be launched as a thread with different
	function. This barrier will be used to synchronize the threads when the dump
	of the databases are finished.
	:type barrier: threading.Barrier
	"""
	# Wait the end of MYSQL_DUMP_COMMAND.
	barrier.wait()

	# Get DURATION records.
	for t in range(max(DURATIONS)):
		# Since data which are in /proc/cpuinfo changes we need to open/close this
		# file every time we want to read the new values.
		cpu_info = open(_CPU_INFO, 'r')

		# Put the whole file in content as a string.
		content = ''.join(cpu_info.readlines())

		string = str(t)

		# Get frequency for each core and store it in string.
		for freq in re.findall('cpu MHz\t\t: (\d+\.\d+)', content):
			string += ';%s' % freq
		string += '\n'

		# Write the new line in the csv file.
		out.write(string)

		# Sleep one second to get cpu frequency each second.
		time.sleep(1)

		cpu_info.close()

# This command is run before each run to force read the whole database and bring
# it in memory.
# WARNING We can not use redirection directly in exec_run. We need to use bash
# to be able to redirect. Thanks Damien for the information and also the
# suggestion of the command.
_MYSQL_DUMP_COMMAND = "bash -c 'mysqldump sbtest > /dev/null'"

# Those keys are stored under the stat json object in the memory_stats object.
_MEMORY_KEYS = ['active_anon', 'active_file', 'inactive_anon', 'inactive_file', 'unevictable', 'pgfault', 'pgmajfault', 'pgpgin', 'pgpgout']

# 'io_serviced_recursive' gives the number of read, write, etc. while
# 'io_service_bytes_recursive' gives the number of bytes read, written, etc.
_IO_KEYS = ['io_serviced_recursive', 'io_service_bytes_recursive']

# The keys added to a temporary dictionary.
_KEYS = ['usage'] + _MEMORY_KEYS + ['io_serviced_recursive:Read', 'io_service_bytes_recursive:Read']

def thread_func(container, command, out, stats, barrier, duration):
	"""Run the commands in container and write the output in out.
	This function will be used as a thread.
	:param container: The container in which command will be run. It must already
	been started.
	:type container: docker.models.containers.Container
	:param command: The command to run in container.
	:type command: str.
	:param out: An open in write file object where the output of command will be
	written.
	:type out: _io.TextIOWrapper
	:param out: An open in write file object where the stats of the container will
	be written.
	:type out: _io.TextIOWrapper
	:param barrier: This function will be launched as a thread with different
	function. This barrier will be used to synchronize the threads when the dump
	of the databases are finished.
	:type barrier: threading.Barrier
	:param duration: Duration of the experiment. It is used to get as many records
	from stats.
	:type duration: int.
	"""
	container.exec_run(_MYSQL_DUMP_COMMAND)

	barrier.wait()

	gen = container.stats()

	# Run the benchmark inside the container and write the output to out.
	# exec_run returns a tuple whom second field contains bytes. The decode
	# translate them to a string.
	out.write(container.exec_run(command)[1].decode('utf-8') + '\n')

	statistics = []
	old_values = {}

	# old_values will be used below, this loop initializes it with 0s and
	# with the good keys.
	# The keys are the two last of KEYS (i/o related) and the four last of
	# MEMORY_KEYS (pgfault, pgmajfault, pgpgin and pgpgout).
	for i in _KEYS[1 + len(_MEMORY_KEYS) - 4:]:
		old_values[i] = 0

	for record in gen:
		# Translate the record in string and translate it to python dictionary.
		# Vive la Joz' ! Bon vivant Docteur bon vivant !
		jason = json.loads(record.decode('utf-8'))

		# Create a new dictionary at each iteration so the statistics's cases are
		# differents.
		chicandier = {'usage': jason['memory_stats']['usage']}

		# Add the interesting memory stats to dictionary.
		# We do not add directly the 4 lasts key because they are accumulation and
		# not instantaneous values.
		for key in _MEMORY_KEYS[:-4]:
			chicandier[key] = jason['memory_stats']['stats'][key]

		# Deal specifically with the 4 lasts.
		for key in _MEMORY_KEYS[-4:]:
			# Get the difference between this iteration and the previous one.
			val = jason['memory_stats']['stats'][key] - old_values[key]

			# Update the old value. Since it begins at 0 the first value will be
			# quite wrong but I do not think it will be a big problem.
			old_values[key] = jason['memory_stats']['stats'][key]

			# Little trick if the key is not already present.
			# TODO Maybe this if is useless since this code is called each second
			# and chicandier is recreated each second. But it works and I will not
			# loose some time for a little branch...
			if key in chicandier:
				chicandier[key] += val
			else:
				chicandier[key] = val

		for key in _IO_KEYS:
			for io in jason['blkio_stats'][key]:
				if io['op'] == 'Read':
					# Reconstruct the key with the jason key and the op.
					true_key = key + ':' + io['op']

					# Get the difference between this iteration and the previous one.
					# The value is the number of read or write for this second. It can be
					# seen as the bandwith.
					val = io['value'] - old_values[true_key]

					# Update the old value. Since it begins at 0 the first value will be
					# quite wrong but I do not think it will be a big problem.
					old_values[true_key] = io['value']

					# Little trick if the key is not already present.
					# TODO Maybe this if is useless since this code is called each second
					# and chicandier is recreated each second. But it works and I will not
					# loose some time for a little branch...
					if true_key in chicandier:
						chicandier[true_key] += val
					else:
						chicandier[true_key] = val

		statistics.append(chicandier)

		# Our sysbenches last duration seconds so we just want duration records
		# since containerd give stats every second.
		if len(statistics) == duration:
			break

	# Add the statistics to the stat file.
	for i in range(len(statistics)):
		stats.write('%d' % i)

		# Write each statistics in order.
		for key in _KEYS:
			stats.write(';%d' % statistics[i][key])

		stats.write('\n')

def prepare(containers, chars, directory, outs, stats, drop_cache):
	"""Prepare the benchmark by executing a warmup command.
	This function will also open files used to contain output of benchmark and
	container statistics.
	This function will raise an exception if the list given as parameters do not
	have the same length.
	:param containers: The list of containers which will run the benchmark.
	:type containers: list<docker.models.containers.Container>.
	:param chars: A list of string which contains identifier of containers.
	:type chars: list<str>.
	:param directory: A string which contains the directory where files will be
	stored.
	:type directory: str.
	:param outs: An empty list which at the end of this function execution will
	contain _io.TextIOWrapper. They will be used later to store output of
	benchmark.
	:type: list.
	:param stats: An empty list which at the end of this function execution will
	contain _io.TextIOWrapper. They will be used later to store containers'
	statistics.
	:type: list.
	:param drop_cache: A file handler for /proc/sys/vm/drop_caches open in write
	mode.
	:type drop_cache: _io.TextIOWrapper.
	"""
	if len(containers) != len(chars):
		raise Exception('containers and chars must have the same length')

	# Run each benchmark once so they are prepared.
	for i in range(len(containers)):
		containers[i].exec_run(_SYSBENCH_PREPARE_COMMAND)

		# Drop the cache so the preparation above does not have a consequence on the
		# runs below.
		drop_cache.write(bytearray('3\n', 'utf-8'))

		outs.append(open('%s/container_%c_%s.out' % (directory, chars[i], containers[i].name), 'w'))
		stats.append(open('%s/container_%c_%s.stats' % (directory, chars[i], containers[i].name), 'w'))

		# Write CSV header.
		stats[i].write('iteration;usage;active_anon;active_file;inactive_anon;inactive_file;unevictable;pgfault;pgmajfault;pgpgin;pgpgout;reads;read_bandwith\n')

# We will run 10 runs.
_RUNS = 1

# The following commands are use to interact with trace-cmd.
# This tools is an UI to interact with in-kernel ftrace.
# TRACE_CMD_START will start recording the time passed in
#mem_cgroup_soft_limit_reclaim.
_TRACE_CMD_START = 'trace-cmd start -p function_graph -l mem_cgroup_soft_limit_reclaim'

# TRACE_CMD_STOP will stop the recording.
_TRACE_CMD_STOP = 'trace-cmd stop'

# TRACE_CMD_EXTRACT will read sysfs and get the information.
_TRACE_CMD_EXTRACT = 'trace-cmd extract -o %s'

# TRACE_CMD_RESET will stop ftrace so the kernel will run without any overhead.
_TRACE_CMD_RESET = 'trace-cmd reset'

def run(directory, containers, run_commands, outs, stats, drop_cache, proc_stat_out, durations):
	"""Run the experiment by running 10 times the benchmark to compute mean and
	standard deviation.
	This function will raise an exception if the list given as parameters do not
	have the same length.
	:param directory: The directory where files will be stored.
	:type directory: str.
	:param containers: The list of containers which will run the benchmark.
	:type containers: list<docker.models.containers.Container>.
	:param run_commands: A list of command to run. Basically, each container will
	run a command.
	:type run_commands: list<str>.
	:param outs: A list of files which will contain the output of commands ran by
	containers. There are as many files as containers.
	:type outs: list<_io.TextIOWrapper>.
	:param stats: A list of files which will contain the stats of containers.
	:type stats: list<_io.TextIOWrapper>.
	:param drop_cache: A file handler for /proc/sys/vm/drop_caches open in write
	mode.
	:type drop_cache: _io.TextIOWrapper.
	:param proc_stat_out: A file handler open in write mode. It will contain
	output of /proc/stat.
	:type proc_stat_out: _io.TextIOWrapper.
	:param durations: A list of integers which indicates the experiment duration
	for each containers.
	:type durations: list<int>
	"""
	# Set contains unique value so if there are two differents items in this set
	# it means that lenghts are not the same!
	if len(set([len(containers), len(run_commands), len(outs), len(stats), len(durations)])) != 1:
		raise Exception('containers (%d), run_commands (%d), outs (%d), stats (%d) and durations (%d) must have the same length' % (len(containers), len(run_commands), len(outs), len(stats), len(durations)))

	for i in range(_RUNS):
		threads = []
		proc_stat = open('/proc/stat')

		# Declare a threading barrier which will wait as many threads as containers.
		barrier = threading.Barrier(len(containers))

		for j in range(len(containers)):
			# Prepare the threads which will run filebench inside the containers.
			threads.append(threading.Thread(target = thread_func, args = (containers[j], run_commands[j], outs[j], stats[j], barrier, durations[j])))

		subprocess.call(_TRACE_CMD_START.split())

		# Launch the threads.
		for t in threads:
			t.start()

		# Wait them.
		for t in threads:
			t.join()

		subprocess.call(_TRACE_CMD_STOP.split())
		subprocess.call((_TRACE_CMD_EXTRACT % ("%s/trace_extraction_%d.dat" % (directory, i))).split())
		subprocess.call(_TRACE_CMD_RESET.split())

		proc_stat_out.write(proc_stat.read())

		# Drop the cache between each run so they are independents.
		drop_cache.write(bytearray('3\n', 'utf-8'))

		# Force write to files after each run so we can collect stats during
		# the experiment.
		for j in range(len(containers)):
			outs[j].flush()
			stats[j].flush()

			proc_stat_out.flush()
			proc_stat.close()

		print("[ %s ] Run #%d finished!" % (time.ctime(), i))

def clean(containers, outs, stats, drop_cache, proc_stat_out):
	"""Stop containers, close open files and delete all containers' volumes.
	:param containers: A list of containers which will be stopped.
	:type containers: list<docker.models.containers.Container>.
	:param outs: A list of files to close.
	:type outs: list<_io.TextIOWrapper>.
	:param stats: A list of files to close.
	:type stats: list<_io.TextIOWrapper>.
	:param drop_cache: A file to close.
	:type drop_cache: _io.TextIOWrapper.
	:param proc_stat_out: A file to close.
	:type proc_stat_out: _io.TextIOWrapper.
	"""
	# We only print an error message if they do not have the same length because
	# we will still 'clean' what were given as parameters.
	if len(containers) != len(outs) or len(containers) != len(stats) or len(outs) != len(stats):
		print('containers, outs and stats must have the same length', file = sys.stderr)

	# Stop,remove and close everything.
	for i in range(len(containers)):
		containers[i].stop()
		containers[i].remove()

		outs[i].close()
		stats[i].close()

	drop_cache.close()
	proc_stat_out.close()

	# Delete all volumes.
	docker.from_env().volumes.prune()
#! /usr/bin/env bash
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>


python3 alone_default.py
echo 'Finished run alone_default.py!'
# python3 alone_max.py 1800M
# echo 'Finished run alone_max_limit.py with 1800M!'
# python3 alone_max.py 1000M
# echo 'Finished run alone_max_limit.py with 1000M!'
python3 soft_limit.py 900M 800M 600M 500M
echo 'Finished run soft_limit.py with 900M/800M/600M/500M!'
python3 default.py
echo 'Finished run default.py!'
python3 max_limit.py 900M 800M 600M 500M
echo 'Finished run max_limit.py with 900M/800M/600M/500M!'
python3 mechanism.py 900M 800M 600M 500M 2
echo 'Finished run max_limit.py with 900M/800M/600M/500M!'
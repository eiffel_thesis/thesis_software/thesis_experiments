#! /usr/bin/env bash
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>


# for i in 2 5 10 15; do
for i in 2; do
# 	python3 alone_mechanism.py 1000M $i
# 	echo "Finished run alone_mechanism with 1000M and ${i}%!"

	python3 mechanism.py 1000M 800M 600M 400M $i
	echo "Finished run mechanism.py with 1000M/800M/600M/400M and ${i}%!"
done

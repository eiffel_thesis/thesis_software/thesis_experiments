#! /usr/bin/env python3
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
import sys
import docker
import os
import time
import common


"""This python script will start two containers with sysbench with the given as
argument soft limits.

It will then run 10 times the SYSBENCH_CONTAINER_A and SYSBENCH_CONTAINER_B in
each container as argument. The output of filebench will be written to a file
and statistics (memory usage in bytes and reads from the disk) will be written
to another file.
"""

CHARS = ['A', 'B']

def main():
	containers = []
	outs = []
	stats = []

	if len(sys.argv) < 4:
		sys.exit("Usage: %s soft_limit_A soft_limit_B percent_to_take" % sys.argv[0])

	client = docker.from_env()

	for c in CHARS:
		# Launch containers as detached, to use new kernel feature we also need to
		# run the containers as privileged.
		containers.append(client.containers.run('sysbench', detach = True, privileged = True, mem_limit = '3G'))

	# Wait 120 seconds so the sysbench container is ready. Indeed, it can be slow
	# because mysql is long to boot.
	time.sleep(120)

	# Open /proc/sys/vm/drop_caches to be able to drop linux page cache.
	drop_cache = open('/proc/sys/vm/drop_caches', 'wb', buffering = 0)

	# Every files created in this script will be stored in this directory.
	# Due to the concatenation of the date it should be unique.
	directory = '%s/%s-%s-%s' % (os.path.expanduser('~'), sys.argv[0], sys.argv[3], time.ctime().replace(' ', '_'))

	os.mkdir(directory)

	# This file will store the content of /proc/stat.
	# We want this obtain the time passed in kernel.
	proc_stat_out = open('%s/%s_proc_stat' % (directory, sys.argv[0]), 'w')

	try:
		common.prepare(containers, CHARS, directory, outs, stats, drop_cache)

		# Update containers' limits and set divider.
		for i in range(len(containers)):
			containers[i].update(mem_reservation = sys.argv[i + 1], mem_limit = '3G', memswap_limit = '3G', cpuset_cpus = common.CONTAINERS_CPUS_SETS[i])

			percent_file = open('/sys/fs/cgroup/memory/docker/%s/memory.percent_to_take' % containers[i].id, 'wb', buffering = 0)
			percent_file.write(bytearray('%s\n' % sys.argv[3], 'utf-8'))
			percent_file.close()

		# Run the experiment.
		common.run(directory, containers, common.SYSBENCH_RUN_COMMANDS, outs, stats, drop_cache, proc_stat_out, common.DURATIONS)
	finally:
		# If an exception occurs we will not treat it particularly because the whole
		# run needs to be run again.
		# So just printing the callgraph is OK.
		# But either an exception occurs or not we need to clean everything.
		common.clean(containers, outs, stats, drop_cache, proc_stat_out)

if __name__ == "__main__":
	main()

#! /usr/bin/env bash
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>

python3 test_soft_limit.py 2000M 1000M
echo 'Finished run 2G/1G!'
python3 test_soft_limit.py 2500M 500M
echo 'Finished run 2.3G/.7G!'
python3 test_soft_limit.py 2300M 700M
echo 'Finished run 2.3G/.7G!'
python3 test_soft_limit.py 2300M 1300M
echo 'Finished run 2.3G/1.3G!'
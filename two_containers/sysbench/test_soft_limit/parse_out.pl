#! /usr/bin/env perl
# SPDX-License-Identifier: MPL-2.0
use strict;
use warnings;

my $filename;

my $fd_in;
my $fd_out;

my $i;

$filename = pop or die "Usage: $0 file";

open $fd_in, '<', $filename or die "Can not open '${filename}': $!";
open $fd_out, '>', "${filename}.csv" or die "Can not open '${filename}.csv': $!";

print {$fd_out} "run;queries;latency\n";

$i = 0;

while(<$fd_in>){
	# Get the total number of queries done to the database of each sysbench
	# oltp prints.
	if($_ =~ m-queries: +(\d+)-){
		print {$fd_out} $i++ . ";${1};";
	}

	# Get the 95th percentile latency of each sysbench oltp prints.
	if($_ =~ m-95th percentile: +(\d+.\d+)-){
		print {$fd_out} "${1}\n";
	}
}

close $fd_in or warn "Problem while closing '${filename}': $!";
close $fd_out or warn "Problem while closing '${filename}.csv': $!";
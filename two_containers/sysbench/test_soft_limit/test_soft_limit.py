#! /usr/bin/env python3
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
import sys
import docker
import os
import threading
import json
import time

"""This python script will start two containers with filebench with the given as
argument soft limits.

It will then run 10 times the SYSBENCH_CONTAINER_A and SYSBENCH_CONTAINER_B in
each container as argument. The output of filebench will be written to a file
and statistics (memory usage in bytes and reads from the disk) will be written
to another file.
"""

# Each run lasts 120 seconds.
DURATION = 120

# Sysbench commands for the container A and B.
# Container A's db will be 2G and that of B will be 1G. Each will execute with
# two threads since they have 2 cores.
SYSBENCH_PREPARE_COMMANDS = ['sysbench --threads=2 /usr/share/sysbench/oltp_read_only.lua --table_size=10000000 prepare', 'sysbench --threads=2 /usr/share/sysbench/oltp_read_only.lua --table_size=5000000 prepare']
SYSBENCH_RUN_COMMANDS = ['sysbench --time=%d --threads=2 /usr/share/sysbench/oltp_read_only.lua --table_size=10000000 run' % DURATION, 'sysbench --time=%d --threads=2 /usr/share/sysbench/oltp_read_only.lua --table_size=5000000 run' % DURATION]

# This command is run before each run to force read the whole database and bring
# it in memory.
# WARNING We can not use redirection directly in exec_run. We need to use bash
# to be able to redirect. Thanks Damien for the information and also the
# suggestion of the command.
MYSQL_DUMP_COMMAND = "bash -c 'mysqldump sbtest > /dev/null'"

# Give two virtual core to each container. The virtual core are chosen so they
# are on the same physical core (i.e cache are shared).
CONTAINERS_CPUS_SETS = ['0,2', '1,3']

CHARS = ['A', 'B']

# The keys added to a temporary dictionary.
KEYS = ['Usage', 'io_serviced_recursive:Read', 'io_service_bytes_recursive:Read']

# 'io_serviced_recursive' gives the number of read, write, etc. while
# 'io_service_bytes_recursive' gives the number of bytes read, written, etc.
IO_KEYS = ['io_serviced_recursive', 'io_service_bytes_recursive']

def thread_func(container, command, out, stats):
	"""Run the command in container and write the output in out.
	This function will be used as a thread.
	:param container: The container in which command will be run. It must already
	been started.
	:type container: docker.models.containers.Container
	:param command: The command to run in container.
	:type command: str
	:param out: An open in write file object where the output of command will be
	written.
	:type out: _io.TextIOWrapper
	:param out: An open in write file object where the stats of the container will
	be written.
	:type out: _io.TextIOWrapper
	"""
	container.exec_run(MYSQL_DUMP_COMMAND)

	gen = container.stats()

	# Run the benchmark inside the container and write the output to out.
	# exec_run returns a tuple whom second field contains bytes. The decode
	# translate them to a string.
	out.write(container.exec_run(command)[1].decode('utf-8') + '\n')

	statistics = []
	old_values = {}

	# old_values will be used below, this loop initializes it with 0s and
	# with the good keys.
	for i in KEYS[1:]:
		old_values[i] = 0

	for record in gen:
		# Translate the record in string and translate it to python dictionary.
		# Vive la Joz' ! Bon vivant Docteur bon vivant !
		jason = json.loads(record.decode('utf-8'))

		# Create a new dictionary at each iteration so the statistics's cases are
		# differents.
		chicandier = {'Usage': jason['memory_stats']['usage']}

		for key in IO_KEYS:
			for io in jason['blkio_stats'][key]:
				if io['op'] == 'Read':
					# Reconstruct the key with the jason key and the op.
					true_key = key + ':' + io['op']

					# Get the difference between this iteration and the previous one.
					# The value is the number of read or write for this second. It can be
					# seen as the bandwith.
					val = io['value'] - old_values[true_key]

					# Update the old value. Since it begins at 0 the first value will be
					# quite wrong but I do not think it will be a big problem.
					old_values[true_key] = io['value']

					# Little trick if the key is not already present.
					# TODO Maybe this if is useless since this code is called each second
					# and chicandier is recreate each second. But it works and I will no
					# loose some time for a little if...
					if true_key in chicandier:
						chicandier[true_key] += val
					else:
						chicandier[true_key] = val

		statistics.append(chicandier)

		# Our filebenches last DURATION seconds so we just want DURATION
		# records since containerd give stats every second.
		if len(statistics) == DURATION:
			break

	# Add the statistics to the stat file.
	for i in range(len(statistics)):
		stats.write('%d' % i)

		# Write each statistics in order.
		for key in KEYS:
			stats.write(';%d' % statistics[i][key])

		stats.write('\n')

def main():
	containers = []
	outs = []
	stats = []

	if len(sys.argv) != 3:
		sys.exit("Usage: %s soft_limit0 soft_limit1" % sys.argv[0])

	client = docker.from_env()

	for i in range(len(sys.argv) - 1):
		# Launch containers as detached.
		containers.append(client.containers.run('sysbench', auto_remove = True, detach = True, mem_limit = '3G'))

	# Wait 30 seconds so the sysbench container is ready. Indeed, it can be slow
	# because mysql is long to boot.
	time.sleep(30)

	# Open /proc/sys/vm/drop_caches to be able to drop linux page cache.
	drop_cache = open('/proc/sys/vm/drop_caches', 'wb', buffering = 0)

	# Run each benchmark once so they are prepared.
	for i in range(len(containers)):
		containers[i].exec_run(SYSBENCH_PREPARE_COMMANDS[i])

		# Set the container limits once its preparation phase is finished.
		# sys.argv[i + 1] because sys.argv[0] is the program name and there are
		# len(sys.argv) - 1 containers.
		containers[i].update(mem_reservation = sys.argv[i + 1], mem_limit = '3G', memswap_limit = '3G', cpuset_cpus = CONTAINERS_CPUS_SETS[i])

		# Drop the cache so the preparation above does not have a consequence on the
		# runs below.
		drop_cache.write(bytearray('3\n', 'utf-8'))

		outs.append(open(os.path.expanduser('~/container_%c_%s_%s.out' % (CHARS[i], containers[i].name, sys.argv[i + 1])), 'w'))
		stats.append(open(os.path.expanduser('~/container_%c_%s_%s.stats' % (CHARS[i], containers[i].name, sys.argv[i + 1])), 'w'))

		stats[i].write('iteration;usage;reads;read_bandwith\n')

	# Run each filebench 10 times to compute mean and standard deviation.
	for i in range(10):
		threads = []

		for j in range(len(containers)):
		# Prepare the threads which will run filebench inside the containers.
			threads.append(threading.Thread(target = thread_func, args = (containers[j], SYSBENCH_RUN_COMMANDS[j], outs[j], stats[j])))

		# Launch the threads.
		for t in threads:
			t.start()

		# Wait them.
		for t in threads:
			t.join()

		# Drop the cache between each run so they are independents.
		drop_cache.write(bytearray('3\n', 'utf-8'))

	# Stop and close everything.
	for i in range(len(containers)):
		containers[i].stop()

		outs[i].close()
		stats[i].close()

	drop_cache.close()

if __name__ == "__main__":
    main()
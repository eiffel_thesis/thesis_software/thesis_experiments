#! /usr/bin/env bash
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>


python3 extended_test_soft_limit_alone_vmachine.py 1500M
echo 'Finished run extended_test_soft_limit_alone with 1500M'
python3 extended_test_soft_limit_vmachine_without_soft_limit.py
echo 'Finished run extended_test_without_soft_limit without soft limit!'
python3 extended_test_soft_limit_vmachine.py 1500M 1300M
echo 'Finished run extended_test_soft_limit with 1500M/1300M!'
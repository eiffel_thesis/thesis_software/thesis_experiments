#! /usr/bin/env perl
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
# SPDX-License-Identifier: MPL-2.0
use strict;
use warnings;

my $filename;
my $fd;

my @array;
my $begin_time;

@array = ();

$filename = shift or die "Usage: $0 file.trace";

open $fd, '<', $filename or die "Problem while opening $filename: $!";

while(<$fd>){
	if($_ =~ /(\d+)\.\d+: funcgraph_entry:\s+.?\s(\d+\.\d+) us/){
		my $idx;

		if(!defined $begin_time){
			$begin_time = $1;
		}

		$idx = $1 - $begin_time;

		if(defined $array[$idx]){
			$array[$idx]->{'time'} += $2;
			$array[$idx]->{'calls'}++;
		}else{
			$array[$idx] = {'time' => $2, 'calls' => 1};
		}
	}
}

close $fd or warn "Problem while closing $filename: $!";

$filename = "${filename}.time.csv";

open $fd, '>', $filename or die "Problem while opening $filename: $!";

print $fd "time;time_passed_in;calls\n";

foreach my $i (0..$#array){
	my $str;

	$str = "$i;";

	if(defined $array[$i]){
		$str .= $array[$i]->{'time'} . ";" . $array[$i]->{'calls'};
	}else{
		$str .= '0;0'
	}

	print $fd "$str\n";
}

close $fd or warn "Problem while closing $filename: $!";
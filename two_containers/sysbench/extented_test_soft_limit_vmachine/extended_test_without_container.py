#! /usr/bin/env python3
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
import sys
import os
import threading
import json
import time
import multiprocessing
import re
import subprocess


"""This python script will start two process with sysbench.

It will then run 10 times the SYSBENCH_CONTAINER_A and SYSBENCH_CONTAINER_B in
each container as argument. The output of filebench will be written to a file
and statistics (memory usage in bytes and reads from the disk) will be written
to another file.
"""

# Each run lasts 900 seconds.
DURATION = 900

CHARS = ['A', 'B']

MYSQL_CREATE_USER = ['mysql', '-e', 'create user if not exists sbtest;']
MYSQL_DB_CREATE = ['mysql', '-e', 'create database if not exists %c;']
MYSQL_GRANT_PRIVILEGES = ['mysql', '-e', "grant all privileges on %c.* to 'sbtest';"]

# Process' DB will be 4G.
# To check the DB size this mysql command can be used (https://stackoverflow.com/a/47629547):
# SELECT table_schema AS "Database", SUM(data_length + index_length) / 1024 / 1024 AS "Size (MB)" FROM information_schema.TABLES GROUP BY table_schema;
# Each will execute with two threads since they have 2 cores.
SYSBENCH_PREPARE_COMMAND = 'sysbench --mysql-db=%c --threads=2 /usr/local/share/sysbench/oltp_read_only.lua --table_size=20000000 prepare'

SYSBENCH_CLEANUP_COMMAND = 'sysbench --mysql-db=%c --threads=2 /usr/local/share/sysbench/oltp_read_only.lua --table_size=20000000 cleanup'

# Each experiment will be composed of 5 scenarios.
# To be able to do that sysbench was modified and arguments '--scheduled-time'
# and '--scheduled-rate' were introduced.
SYSBENCH_RUN_COMMANDS = {'A' : ['sysbench --mysql-db=A --threads=2 --time=900 --rate=0 --scheduled-rate=0,200 --scheduled-time=540,900 --report-interval=1 --report_checkpoints=180,360,540,720 /usr/local/share/sysbench/oltp_read_only.lua --table_size=20000000 run'], 'B' : ['sysbench --mysql-db=B --threads=2 --time=900 --rate=0 --scheduled-rate=0,200,0,200,0 --scheduled-time=180,360,540,720,900 --report-interval=1 --report_checkpoints=180,360,540,720 /usr/local/share/sysbench/oltp_read_only.lua --table_size=20000000 run']}

# This command is run before each run to force read the whole database and bring
# it in memory.
MYSQL_DUMP_COMMAND = ['bash', '-c', 'mysqldump %c > /dev/null']

def format_list(list, char):
	"""Find a formattable string in the list and format it with char.
	:param list: A list of string which can contains formattable strings.
	:type list: list of str.
	:param char: If a string from list can be formatted it will be formatted with
	this argument.
	:type char: str.
	:return: Return the list but with formatted string.
	"""
	ret = []

	for str in list:
		if '%c' in str:
			str = str % char

		ret.append(str)

	return ret

def thread_func(char, commands, out, barrier):
	"""Run the commands in container and write the output in out.
	This function will be used as a thread.
	:param char: The char used to identify the process.
	:type char: str
	:param commands: The commands to run in subprocess.
	:type commands: list of str.
	:param out: An open in write file object where the output of command will be
	written.
	:type out: _io.TextIOWrapper
	:param barrier: This function will be launched as a thread with different
	function. This barrier will be used to synchronize the threads when the dump
	of the databases are finished.
	:type barrier: threading.Barrier
	"""
	subprocess.call(format_list(MYSQL_DUMP_COMMAND, char))

	barrier.wait()

	for command in commands:
		# Run the benchmark inside a subprocess and write the output to out.
		# exec_run returns a tuple whom second field contains bytes. The decode
		# translate them to a string.
		subprocess.call(command.split(), stdout = out)

def main():
	outs = []

	# Open /proc/sys/vm/drop_caches to be able to drop linux page cache.
	drop_cache = open('/proc/sys/vm/drop_caches', 'wb', buffering = 0)

	# Every files created in this script will be stored in this directory.
	# Due to the concatenation of the date it should be unique.
	directory = '%s/%s-%s' % (os.path.expanduser('~'), sys.argv[0], time.ctime().replace(' ', '_'))

	os.mkdir(directory)

	# Create default sysbench user.
	subprocess.call(MYSQL_CREATE_USER)

	# Run each benchmark once so they are prepared.
	for c in CHARS:
		# Create database for each process.
		subprocess.call(format_list(MYSQL_DB_CREATE, c))

		# Give all privileges on just created database to default sysbench user.
		subprocess.call(format_list(MYSQL_GRANT_PRIVILEGES, c))

		# Prepare the table in the database.
		subprocess.call((SYSBENCH_PREPARE_COMMAND % c).split())

		# Drop the cache so the preparation above does not have a consequence on the
		# runs below.
		drop_cache.write(bytearray('3\n', 'utf-8'))

		outs.append(open('%s/process_%c.out' % (directory, c), 'w'))

	# Run each sysbench 10 times to compute mean and standard deviation.
	for i in range(10):
		threads = []

		# Declare a threading barrier which will wait 3 threads: two for container A
		# and B.
		barrier = threading.Barrier(len(CHARS))

		for j in range(len(CHARS)):
			# Prepare the threads which will run sysbench inside the process.
			threads.append(threading.Thread(target = thread_func, args = (CHARS[j], SYSBENCH_RUN_COMMANDS[CHARS[j]], outs[j], barrier)))

		# Launch the threads.
		for t in threads:
			t.start()

		# Wait them.
		for t in threads:
			t.join()

		# Drop the cache between each run so they are independents.
		drop_cache.write(bytearray('3\n', 'utf-8'))

		# Force write to files after each run so we can collect stats during
		# the experiment.
		for j in range(len(CHARS)):
			outs[j].flush()

		print("[ %s ] Run #%d finished!" % (time.ctime(), i))

	# Clean and close everything.
	for i in range(len(CHARS)):
		subprocess.call((SYSBENCH_CLEANUP_COMMAND % CHARS[i]).split())

		outs[i].close()

	drop_cache.close()

if __name__ == "__main__":
	main()
#! /usr/bin/env bash
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>

python3 advanced_test_soft_limit.py 2000M 2000M
echo 'Finished run advanced_test_soft_limit with 2G/2G!'
python3 with_soft_limit_update.py 2000M 2000M
echo 'Finished run with_soft_limit_update with 2G/2G!'
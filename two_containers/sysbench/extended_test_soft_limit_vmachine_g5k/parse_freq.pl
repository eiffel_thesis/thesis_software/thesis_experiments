#! /usr/bin/env perl
# SPDX-License-Identifier: MPL-2.0
use strict;
use warnings;

# Experiment lasts 900 seconds so there will 900 records from 0 to 899.
my $NR_RECORDS = 899;

# The keys of our hash.
my @KEYS = ('cpu0_frequency', 'cpu1_frequency', 'cpu2_frequency', 'cpu3_frequency');

my $filename;

my %dict;

my $fd_in;
my $fd_out;

$filename = pop or die "Usage: $0 file";

open $fd_in, '<', $filename or die "Can not open '${filename}': $!";
open $fd_out, '>', "${filename}.csv" or die "Can not open '${filename}.csv': $!";

# Print the header.
print {$fd_out} 'time';
for my $key (@KEYS){
	printf {$fd_out} ';%s_avg;%s_std', $key, $key;
}
print {$fd_out} "\n";

%dict = ();

foreach my $key (@KEYS){
	$dict{$key} = [];

	foreach my $i (0..$NR_RECORDS){
		# The array of our keys will be array of array. The array of array will
		# contain all the value for a given 'time'.
		push @{$dict{$key}}, [];
	}
}

while(<$fd_in>){
	# Store the statistics in the corresponding array.
	if(my @matches = ($_ =~ m-(\d+);(\d+\.\d+);(\d+\.\d+);(\d+\.\d+);(\d+\.\d+)-g)){
		for my $i (1 .. scalar @matches - 1){
			# $matches[0] is the iteration and $matches[$i] is the value for
			# corresponding key $KEYS[$i - 1].
			push @{$dict{$KEYS[$i - 1]}[$matches[0]]}, $matches[$i];
		}
	}
}

foreach my $i (0..$NR_RECORDS){
	my $to_write;

	$to_write = "${i}";

	# Poor man "generic" loop on statistics (each key is a statistic).
	foreach my $key (@KEYS){
		my $avg;
		my $std;
		my $len;

		$avg = 0;
		$std = 0;

		# Single loop mean and standard deviation computing:
		# https://www.strchr.com/standard_deviation_in_one_pass
		foreach my $j (@{$dict{$key}[$i]}){
			$avg += $j;

			$std += $j * $j;
		}

		$len = scalar @{$dict{$key}[$i]};
		print "$key;$i" if ($len == 0);
		$avg /= $len;

		$std /= $len;
		$std -= $avg * $avg;

		# If standard deviation is very small it is possible than its value becomes
		# negative thanks to the float arithmetic.
		# So we use abs here as a safeguard.
		$std = sqrt abs $std;

		$to_write .= ";${avg};${std}";
	}

	$to_write .= "\n";

	print {$fd_out} $to_write;
}

close $fd_in or warn "Problem while closing '${filename}': $!";
close $fd_out or warn "Problem while closing '${filename}.csv': $!";
#! /usr/bin/env perl
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
# SPDX-License-Identifier: MPL-2.0
use strict;
use warnings;


my $filename;
my $dir;
my $fd;

my @array;

my $time;
my $calls;

@array = ();

while($filename = shift){
	$time = 0;
	$calls = 0;

	open $fd, '<', $filename or die "Problem while opening $filename: $!";

	if(!defined $dir){
		if($filename =~ m-(\w+)/.*-){
			$dir = $1;
		}
	}

	while(<$fd>){
		if($_ =~ /(\d+\.\d+) us/){
			$time += $1;
			$calls++;
		}
	}

	close $fd or warn "Problem while closing $filename: $!";

	# Time collected in the file is given in µs, we need to translate it.
	push @array, {'time' => $time * 10 ** -6 , 'calls' => $calls};
}

$filename = "${dir}/${dir}.trace.csv";

open $fd, '>', $filename or die "Problem while opening $filename: $!";

print $fd "iteration;time;calls\n";

foreach my $i (0..$#array){
	my $str;

	$str = "$i;";

	if(defined $array[$i]){
		$str .= $array[$i]->{'time'} . ";" . $array[$i]->{'calls'};
	}else{
		$str .= '0;0'
	}

	print $fd "$str\n";
}

close $fd or warn "Problem while closing $filename: $!";
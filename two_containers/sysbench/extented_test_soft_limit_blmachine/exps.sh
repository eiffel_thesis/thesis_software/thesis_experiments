#! /usr/bin/env bash
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>

hungry_mem=$(echo "13.75 * 1024 ^ 3" | bc)

# Fix frequency to 2.8 GHz.
bash fix_frequency.sh 2800000

python3 extended_test_soft_limit_alone_blmachine.py 768M $hungry_mem
echo 'Finished run extended_test_soft_limit_alone with 768M'
python3 extended_test_soft_limit_blmachine.py 768M $hungry_mem
echo 'Finished run extended_test_soft_limit with 768M/768M/14G!'
#! /usr/bin/env bash
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
# SPDX-License-Identifier: MPL-2.0

for i in period_*/*.out; do
	perl parse_out.pl $i
done

for i in period_*/*.stats; do
	perl parse_stats.pl $i
done

for i in period_*/*_proc_stat; do
	perl parse_proc_stat.pl $i
done

for i in period_*; do
	cd $i

	# The following perl script need to parse the ouput of trace-cmd report so we
	# firstly need to create it.
	for j in *.dat; do
		echo "$i/$j"
		trace-cmd report -i $j > $j.trace
	done

	cd ..

	perl parse_trace.pl $i/*.trace

	# Parse the 5th files because it is the "middle" of the files.
	perl parse_trace_time.pl $i/*_5*.trace
done
#! /usr/bin/env perl
use strict;
use warnings;
use POSIX;

our $SC_CLK_TCK;

my $filename;
my $fd;

my @matches;
my $str;

defined ($SC_CLK_TCK = POSIX::sysconf(&POSIX::_SC_CLK_TCK)) or perror('sysconf');

$filename = shift or die "Usage: $0 file_to_parse";

open $fd, '<', $filename or die "Problem while opening $filename: $!";

while(<$fd>){
	if($_ =~ /cpu /){
		push @matches, $_;
	}
}

close $fd or warn "Problem while closing $filename: $!";

$filename = $filename . '.csv';

open $fd, '>', $filename or die "Problem while opening $filename: $!";

print $fd "user_time;system_time\n";

$str = $matches[-1];

if($str =~ /cpu  (\d+) \d+ (\d+)/){
	# All times in /proc/stat are given in _SC_CLK_TCK so we need to divide by it
	# to get those times in seconds.
	printf $fd "%d;%d\n", $1 / $SC_CLK_TCK, $2 / $SC_CLK_TCK;
}

close $fd or warn "Problem while closing $filename: $!";
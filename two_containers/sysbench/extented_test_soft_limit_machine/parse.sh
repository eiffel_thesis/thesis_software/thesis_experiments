#! /usr/bin/env bash
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
# SPDX-License-Identifier: MPL-2.0

for i in alone/*.out pair/*.out; do
	perl parse_out.pl $i
done

for i in alone/*.stats pair/*.stats; do
	perl parse_stats.pl $i
done

for i in alone/*.freq pair/*.freq; do
	perl parse_freq.pl $i
done
#! /usr/bin/env bash
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
# SPDX-License-Identifier: MPL-2.0

if [ $EUID -ne 0 ]; then
	echo "${0} must be run as root!" 1>&2

	exit 1
fi

if [ "$#" == "0" ]; then
	echo "Usage: $0 frequency_in_kHz" 1>&2

	exit 1
fi

for i in /sys/devices/system/cpu/cpufreq/*; do
	echo $1 > $i/scaling_max_freq
	echo $1 > $i/scaling_min_freq
done
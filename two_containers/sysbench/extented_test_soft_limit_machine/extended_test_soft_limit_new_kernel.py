#! /usr/bin/env python3
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
import sys
import docker
import os
import threading
import json
import time
import multiprocessing
import re


"""This python script will start two containers with sysbench with the given as
argument soft limits.

It will then run 10 times the SYSBENCH_CONTAINER_A and SYSBENCH_CONTAINER_B in
each container as argument. The output of filebench will be written to a file
and statistics (memory usage in bytes and reads from the disk) will be written
to another file.
"""

# Each run lasts 900 seconds.
DURATION = 900

CHARS = ['A', 'B']

# Containers' DB will be 2G.
# Each will execute with two threads since they have 2 cores.
SYSBENCH_PREPARE_COMMAND = 'sysbench --threads=2 /usr/share/sysbench/oltp_read_only.lua --table_size=10000000 prepare'

# Each experiment will be composed of 5 sysbenchs execution:

SYSBENCH_RATES = ((0, 600), (150, 135), (0, 600), (150, 135), (0, 600))
SYSBENCH_RUN_COMMAND = "bash -c 'sysbench --report-interval=1 --time=180 --threads=2 --rate=%d /usr/share/sysbench/oltp_read_only.lua --table_size=10000000 run | perl probe.pl %d'"

SYSBENCH_RUN_COMMANDS = {'A' : ["bash -c 'sysbench --report-interval=1 --report-checkpoints=180,360 --time=540 --threads=2 /usr/share/sysbench/oltp_read_only.lua --table_size=10000000 run | perl probe.pl 600'", "bash -c 'sysbench --report-interval=1 --report-checkpoints=180 --time=360 --threads=2 --rate=150 /usr/share/sysbench/oltp_read_only.lua --table_size=10000000 run | perl probe.pl 135'"], 'B' : []}

for rate in SYSBENCH_RATES:
	SYSBENCH_RUN_COMMANDS['B'].append(SYSBENCH_RUN_COMMAND % rate)

# This command is run before each run to force read the whole database and bring
# it in memory.
# WARNING We can not use redirection directly in exec_run. We need to use bash
# to be able to redirect. Thanks Damien for the information and also the
# suggestion of the command.
MYSQL_DUMP_COMMAND = "bash -c 'mysqldump sbtest > /dev/null'"

# Give two virtual cores to each container. The virtual cores are chosen so they
# are on the same physical core (i.e. cache are shared).
CONTAINERS_CPUS_SETS = ['0,2', '1,3']

# Those keys are stored under the stat json object in the memory_stats object.
MEMORY_KEYS = ['active_anon', 'active_file', 'inactive_anon', 'inactive_file', 'unevictable', 'pgfault', 'pgmajfault', 'pgpgin', 'pgpgout']

# 'io_serviced_recursive' gives the number of read, write, etc. while
# 'io_service_bytes_recursive' gives the number of bytes read, written, etc.
IO_KEYS = ['io_serviced_recursive', 'io_service_bytes_recursive']

# The keys added to a temporary dictionary.
KEYS = ['usage'] + MEMORY_KEYS + ['io_serviced_recursive:Read', 'io_service_bytes_recursive:Read']

CPU_INFO = '/proc/cpuinfo'

# Declare a threading barrier which will wait 3 threads: two for container A and
# B and one for monitor_cpu_freq.
barrier = threading.Barrier(len(CHARS) + 1)

def monitor_cpu_freq(out):
	"""This function monitors the cores frequencies every second.
	It writes those information in a csv file.
	:param out: An open in write file object where the frequencies will be
	written.
	:type out: _io.TextIOWrapper
	"""
	# Wait the end of MYSQL_DUMP_COMMAND.
	barrier.wait()

	# Get DURATION records.
	for t in range(DURATION):
		# Since data which are in /proc/cpuinfo changes we need to open/close this
		# file every time we want to read the new values.
		cpu_info = open(CPU_INFO, 'r')

		# Put the whole file in content as a string.
		content = ''.join(cpu_info.readlines())

		string = str(t)

		# Get frequency for each core and store it in string.
		for freq in re.findall('cpu MHz\t\t: (\d+\.\d+)', content):
			string += ';%s' % freq
		string += '\n'

		# Write the new line in the csv file.
		out.write(string)

		# Sleep one second to get cpu frequency each second.
		time.sleep(1)

		cpu_info.close()

def thread_func(container, commands, out, stats):
	"""Run the commands in container and write the output in out.
	This function will be used as a thread.
	:param container: The container in which command will be run. It must already
	been started.
	:type container: docker.models.containers.Container
	:param commands: The commands to run in container.
	:type commands: list of str.
	:param out: An open in write file object where the output of command will be
	written.
	:type out: _io.TextIOWrapper
	:param out: An open in write file object where the stats of the container will
	be written.
	:type out: _io.TextIOWrapper
	"""
	container.exec_run(MYSQL_DUMP_COMMAND)

	barrier.wait()

	gen = container.stats()

	for command in commands:
		# Run the benchmark inside the container and write the output to out.
		# exec_run returns a tuple whom second field contains bytes. The decode
		# translate them to a string.
		out.write(container.exec_run(command)[1].decode('utf-8') + '\n')

	statistics = []
	old_values = {}

	# old_values will be used below, this loop initializes it with 0s and
	# with the good keys.
	# The keys are the two last of KEYS (i/o related) and the four last of
	# MEMORY_KEYS (pgfault, pgmajfault, pgpgin and pgpgout).
	for i in KEYS[1 + len(MEMORY_KEYS) - 4:]:
		old_values[i] = 0

	for record in gen:
		# Translate the record in string and translate it to python dictionary.
		# Vive la Joz' ! Bon vivant Docteur bon vivant !
		jason = json.loads(record.decode('utf-8'))

		# Create a new dictionary at each iteration so the statistics's cases are
		# differents.
		chicandier = {'usage': jason['memory_stats']['usage']}

		# Add the interesting memory stats to dictionary.
		# We do not add directly the 4 lasts key because they are accumulation and
		# not instantaneous values.
		for key in MEMORY_KEYS[:-4]:
			chicandier[key] = jason['memory_stats']['stats'][key]

		# Deal specifically with the 4 lasts.
		for key in MEMORY_KEYS[-4:]:
			# Get the difference between this iteration and the previous one.
			val = jason['memory_stats']['stats'][key] - old_values[key]

			# Update the old value. Since it begins at 0 the first value will be
			# quite wrong but I do not think it will be a big problem.
			old_values[key] = jason['memory_stats']['stats'][key]

			# Little trick if the key is not already present.
			# TODO Maybe this if is useless since this code is called each second
			# and chicandier is recreated each second. But it works and I will not
			# loose some time for a little branch...
			if key in chicandier:
				chicandier[key] += val
			else:
				chicandier[key] = val

		for key in IO_KEYS:
			for io in jason['blkio_stats'][key]:
				if io['op'] == 'Read':
					# Reconstruct the key with the jason key and the op.
					true_key = key + ':' + io['op']

					# Get the difference between this iteration and the previous one.
					# The value is the number of read or write for this second. It can be
					# seen as the bandwith.
					val = io['value'] - old_values[true_key]

					# Update the old value. Since it begins at 0 the first value will be
					# quite wrong but I do not think it will be a big problem.
					old_values[true_key] = io['value']

					# Little trick if the key is not already present.
					# TODO Maybe this if is useless since this code is called each second
					# and chicandier is recreated each second. But it works and I will not
					# loose some time for a little branch...
					if true_key in chicandier:
						chicandier[true_key] += val
					else:
						chicandier[true_key] = val

		statistics.append(chicandier)

		# Our filebenches last DURATION seconds so we just want DURATION
		# records since containerd give stats every second.
		if len(statistics) == DURATION:
			break

	# Add the statistics to the stat file.
	for i in range(len(statistics)):
		stats.write('%d' % i)

		# Write each statistics in order.
		for key in KEYS:
			stats.write(';%d' % statistics[i][key])

		stats.write('\n')

def main():
	containers = []
	outs = []
	stats = []

	if len(sys.argv) < 3:
		sys.exit("Usage: %s soft_limit hungry_memory" % sys.argv[0])

	client = docker.from_env()

	for c in CHARS:
		# Launch containers as detached and privileged so they can write in sysfs.
		containers.append(client.containers.run('sysbench', privileged = True, auto_remove = True, detach = True, mem_limit = '3G'))

	# Wait 120 seconds so the sysbench container is ready. Indeed, it can be slow
	# because mysql is long to boot.
	time.sleep(120)

	# Open /proc/sys/vm/drop_caches to be able to drop linux page cache.
	drop_cache = open('/proc/sys/vm/drop_caches', 'wb', buffering = 0)

	# Every files created in this script will be stored in this directory.
	# Due to the concatenation of the date it should be unique.
	directory = '%s/%s-%s' % (os.path.expanduser('~'), sys.argv[0], time.ctime().replace(' ', '_'))

	os.mkdir(directory)

	# This file will store cpus' frequencies along experiment.
	freq = open('%s/%s_pair.freq' % (directory, sys.argv[1]), 'w')

	# Write the header.
	freq.write('time')
	for i in range(multiprocessing.cpu_count()):
		freq.write(';cpu%d_frequency' % i)
	freq.write('\n')

	# Run each benchmark once so they are prepared.
	for i in range(len(containers)):
		containers[i].exec_run(SYSBENCH_PREPARE_COMMAND)

		# Set the container limits once its preparation phase is finished.
		# sys.argv[i + 1] because sys.argv[0] is the program name and there are
		# len(sys.argv) - 1 containers.
		containers[i].update(mem_reservation = sys.argv[1], mem_limit = '3G', memswap_limit = '3G', cpuset_cpus = CONTAINERS_CPUS_SETS[i])

		# Drop the cache so the preparation above does not have a consequence on the
		# runs below.
		drop_cache.write(bytearray('3\n', 'utf-8'))

		outs.append(open('%s/container_%c_%s_%s.out' % (directory, CHARS[i], containers[i].name, sys.argv[1]), 'w'))
		stats.append(open('%s/container_%c_%s_%s.stats' % (directory, CHARS[i], containers[i].name, sys.argv[1]), 'w'))

		stats[i].write('iteration;usage;active_anon;active_file;inactive_anon;inactive_file;unevictable;pgfault;pgmajfault;pgpgin;pgpgout;reads;read_bandwith\n')

	# Launch a container which will consume sys.argv[2] bytes.
	hungry_container = client.containers.run('hungry_memory', sys.argv[2], auto_remove = True, detach = True)

	# Run each filebench 10 times to compute mean and standard deviation.
	for i in range(10):
		threads = []

		for j in range(len(containers)):
			# Prepare the threads which will run filebench inside the containers.
			threads.append(threading.Thread(target = thread_func, args = (containers[j], SYSBENCH_RUN_COMMANDS[CHARS[j]], outs[j], stats[j])))

		# Add the monitor function so cpu frequency will be monitorer while
		# containers run.
		# ALERT We need to add a comma after freq otherwise Python will not
		# considerate (freq) as a tuple!!! Otherwise it will be considered as the
		# object with parenthesis so the type will be the one of object.
		threads.append(threading.Thread(target = monitor_cpu_freq, args = (freq,)))

		# Launch the threads.
		for t in threads:
			t.start()

		# Wait them.
		for t in threads:
			t.join()

		# Drop the cache between each run so they are independents.
		drop_cache.write(bytearray('3\n', 'utf-8'))

		# Force write to files after each run so we can collect stats during
		# the experiment.
		for j in range(len(containers)):
			outs[j].flush()
			stats[j].flush()

		print("[ %s ] Run #%d finished!" % (time.ctime(), i))

	# Stop and close everything.
	for i in range(len(containers)):
		containers[i].stop()

		outs[i].close()
		stats[i].close()

	hungry_container.stop()

	freq.close()
	drop_cache.close()

if __name__ == "__main__":
	main()

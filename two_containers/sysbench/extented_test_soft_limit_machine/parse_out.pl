#! /usr/bin/env perl
# SPDX-License-Identifier: MPL-2.0
use strict;
use warnings;

# Each run lasts 900 seconds.
my $DURATION = 900;

# An experiment is composed of 10 runs.
my $RUNS = 9;

# A run is composed of 5 parts (i.e. 5 different sysbench executions).
my $PARTS = 5;

my @KEYS = ('transactions', 'queries', 'latencies');

my $filename;

my $fd_in;
my $fd_out0;
my $fd_out1;

my %dict;

my $r;
my $i;
my $j;

my $queries;
my $latencies;

$filename = pop or die "Usage: $0 file";

open $fd_in, '<', $filename or die "Can not open '${filename}': $!";
open $fd_out0, '>', "${filename}.csv" or die "Can not open '${filename}.csv': $!";
open $fd_out1, '>', "${filename}.time.csv" or die "Can not open '${filename}.csv': $!";

$queries = '';
$latencies = '';

print {$fd_out0} 'run';
for my $i (0 .. $PARTS - 1){
	$queries .= ";queries_part${i}";
	$latencies .= ";latencies_part${i}";
}
printf {$fd_out0} "%s%s\n", $queries, $latencies;

print {$fd_out1} "time;transactions_avg;transactions_std;queries_avg;queries_std;latencies_avg;latencies_std\n";

foreach my $key (@KEYS){
	$dict{$key} = [];

	foreach my $i (0 .. $RUNS){
		push @{$dict{$key}}, [];
	}
}

$r = 0;
$i = 0;
$j = 0;

$queries = '';
$latencies = '';

while(<$fd_in>){
	my @matches;

	# Get the transactions, queries and latency for this second.
	if(@matches = ($_ =~ m-\[ \d+s.*tps: (\d+\.\d+) qps: (\d+\.\d+).*\(ms,\d+%\): (\d+\.\d+)-g)){
		for my $k (0 .. scalar @matches - 1){
			$dict{$KEYS[$k]}[$r][$i] = $matches[$k];
		}

		$i++;
	}

	# Get the total number of queries done to the database of each sysbench
	# oltp prints.
	if($_ =~ m-queries: +(\d+)-){
		$queries .= ";$1";
	}

	# Get the Xth percentile latency of each sysbench oltp prints.
	if($_ =~ m-\d+th percentile: +(\d+.\d+)-){
		$latencies .= ";$1";

		$j++;
	}

	if($j == $PARTS){
		printf {$fd_out0} "%d%s%s\n", $r, $queries, $latencies;

		# Reset everything since we dealt with as many parts which compose a run.
		$i = 0;
		$j = 0;
		$queries = "";
		$latencies = "";

		# Pass to the next run.
		$r++;
	}
}

close $fd_in or warn "Problem while closing '${filename}': $!";
close $fd_out0 or warn "Problem while closing '${filename}.csv': $!";

for my $i (0 .. $DURATION - 1){
	my $to_write;

	$to_write = "${i}";

	for my $key (@KEYS){
		my $mean;
		my $std;

		my $len;
		my $missed;

		$mean = 0;
		$std = 0;

		$len = scalar @{$dict{$key}};
		$missed = 0;

		for my $r (0 .. $len - 1){
			my $val;

			$val = $dict{$key}[$r][$i];

			if(not defined $val){
				# It is possible that sysbench output lacks some records for the last
				# second (60 for this case).
				# In this case the average will not be done with 10 values but less.
				$missed++;

				next;
			}

			$mean += $val;
			$std += $val * $val;
		}

		$len -= $missed;

		$mean /= $len;

		$std /= $len;
		$std -= $mean * $mean;

		# If standard deviation is very small it is possible than its value becomes
		# negative thanks to the float arithmetic.
		# So we use abs here as a safeguard.
		$std = sqrt abs $std;

		$to_write .= ";${mean};${std}";
	}

	printf {$fd_out1} "%s\n", $to_write;
}

close $fd_out1 or warn "Problem while closing '${filename}.time.csv': $!";
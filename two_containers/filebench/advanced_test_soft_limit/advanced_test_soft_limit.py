#! /usr/bin/env python3
# SPDX-License-Identifier: MPL-2.0
import sys
import docker
import os
import threading
import json

"""This python script will start two containers with filebench with the given as
argument soft limits.

It will then run 10 times the FILEBENCH_CONTAINER_A and FILEBENCH_CONTAINER_B in
each container as argument. The output of filebench will be written to a file
and statistics (memory usage in bytes and reads from the disk) will be written
to another file.
"""

# Filebench command for the container A.
FILEBENCH_CONTAINER_A = './filebench -f workloads/filemicro_seqread.f'

# Filebench command for the container B.
FILEBENCH_CONTAINER_B = './filebench -f workloads/filemicro_seqreadone_randreadother.f'

def thread_func(container, command, out, stats):
	"""Run the command in container and write the output in out.
	This function will be used as a thread.
	:param container: The container in which command will be run. It must already
	been started.
	:type container: docker.models.containers.Container
	:param command: The command to run in container.
	:type command: str
	:param out: An open in write file object where the output of command will be
	written.
	:type out: _io.TextIOWrapper
	:param out: An open in write file object where the stats of the container will
	be written.
	:type out: _io.TextIOWrapper
	"""
	gen = container.stats()

	# Run the benchmark inside the container and write the output to out.
	# exec_run returns a tuple whom second field contains bytes. The decode
	# translate them to a string.
	out.write(container.exec_run(command)[1].decode('utf-8') + '\n')

	statistics = []
	old_read = 0

	for record in gen:
		# Translate the record in string and translate it to python dictionary.
		# Vive la Joz' ! Bon vivant Docteur bon vivant !
		jason = json.loads(record.decode('utf-8'))

		# Create a new dictionary at each iteration so the statistics's cases are
		# differents.
		chicandier = {'usage': jason['memory_stats']['usage']}

		for io in jason['blkio_stats']['io_serviced_recursive']:
			if io['op'] == 'Read':
				# Get the difference between this iteration and the previous one.
				# The value is so the number of read for this second. It can be seen
				# as the bandwith.
				val = io['value'] - old_read

				# Update the old value. Since it begins at 0 the first value will be
				# quite wrong but I do not think it will be a big problem.
				old_read = io['value']

				# Little trick if the key is not already present.
				if 'reads' in chicandier:
					chicandier['reads'] += val
				else:
					chicandier['reads'] = val

		statistics.append(chicandier)

		# Our filebenches last 30 seconds so we just want 30 records since
		# containerd give stats every second.
		if len(statistics) == 30:
			break

	# Add the statistics to the stat file.
	for i in range(len(statistics)):
		stats.write('%d;%d;%d\n' % (i, statistics[i]['usage'], statistics[i]['reads']))

def main():
	if len(sys.argv) != 3:
		sys.exit("Usage: %s soft_limit0 soft_limit1" % sys.argv[0])

	client = docker.from_env()

	# Launch containers as detached, set their max limits to 3G, their soft limits
	# to given arguments.
	container_a = client.containers.run('filebench', auto_remove = True, detach = True, mem_limit = '3G', mem_reservation = sys.argv[1])
	container_b = client.containers.run('filebench', auto_remove = True, detach = True, mem_limit = '3G', mem_reservation = sys.argv[2])

	# Open /proc/sys/vm/drop_caches to be able to drop linux page cache.
	drop_cache = open('/proc/sys/vm/drop_caches', 'wb', buffering = 0)

	# Run each benchmark once so their files will be prepared for the next times
	# and they will begin to read files approximately at the same time.
	container_a.exec_run(FILEBENCH_CONTAINER_A)
	container_b.exec_run(FILEBENCH_CONTAINER_B)

	# Drop the cache so the runs above does not have a consequence on the runs
	# below. It can be a good idea to separate our filebench experiment in two WSL
	# files: one for preparing the run (by creating file) and the other to
	# effectively run the benchmark.
	drop_cache.write(bytearray('3\n', 'utf-8'))

	a_out = open(os.path.expanduser('~/container_a_%s_%s.out' % (container_a.name, sys.argv[1])), 'w')
	b_out = open(os.path.expanduser('~/container_b_%s_%s.out' % (container_b.name, sys.argv[2])), 'w')

	a_stats = open(os.path.expanduser('~/container_a_%s_%s.stats' % (container_a.name, sys.argv[1])), 'w')
	b_stats = open(os.path.expanduser('~/container_b_%s_%s.stats' % (container_b.name, sys.argv[2])), 'w')

	a_stats.write('iteration;usage;reads\n')
	b_stats.write('iteration;usage;reads\n')

	# Run each filebench 10 times to compute mean and standard deviation.
	for i in range(10):
		threads = []

		# Prepare the threads which will run filebench inside the containers.
		threads.append(threading.Thread(target=thread_func, args=(container_a, FILEBENCH_CONTAINER_A, a_out, a_stats)))
		threads.append(threading.Thread(target=thread_func, args=(container_b, FILEBENCH_CONTAINER_B, b_out, b_stats)))

		# Launch the threads.
		for t in threads:
			t.start()

		# Wait them.
		for t in threads:
			t.join()

		# Drop the cache between each run so they are independents.
		drop_cache.write(bytearray('3\n', 'utf-8'))

	# Stop and close everything.
	container_a.stop()
	container_b.stop()

	a_out.close()
	b_out.close()

	a_stats.close()
	b_stats.close()

	drop_cache.close()

if __name__ == "__main__":
    main()
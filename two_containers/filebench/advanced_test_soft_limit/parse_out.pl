#! /usr/bin/env perl
# SPDX-License-Identifier: MPL-2.0
use strict;
use warnings;

my $filename;

my $fd_in;
my $fd_out;

my $i;

$filename = pop or die "Usage: $0 file";

open $fd_in, '<', $filename or die "Can not open '${filename}': $!";
open $fd_out, '>', "${filename}.csv" or die "Can not open '${filename}.csv': $!";

print {$fd_out} "run;ops/s\n";

$i = 0;

while(<$fd_in>){
	# Get the number for ops/s for the IO Summary of each filebench prints.
	if($_ =~ m-"opname":"IO Summary",(.*, "ops/s":(\d+.\d+))-){
		$i++;

		# Store in the csv file.
		print {$fd_out} "${i};${2}\n";
	}
}

close $fd_in or warn "Problem while closing '${filename}': $!";
close $fd_out or warn "Problem while closing '${filename}.csv': $!";
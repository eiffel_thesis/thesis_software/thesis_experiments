#! /usr/bin/env bash
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>

# Run the container pair with 3 soft limit configurations.
python3 advanced_test_soft_limit.py 1G 1G
echo 'Finished run 1G/1G!'
# It is better to use 1500M instead of 1.5G because docker-py will failed to
# translate 1.5 into an int and will raise an exception.
python3 advanced_test_soft_limit.py 1500M 1500M
echo 'Finished run 1.5G/1.5G!'
python3 advanced_test_soft_limit.py 1G 2G
echo 'Finished run 1G/2G!'
python3 advanced_test_soft_limit.py 2G 1G
echo 'Finished run 2G/1G!'
python3 advanced_test_soft_limit.py 2500M 500M
echo 'Finished run 2.5G/.5G!'
python3 advanced_test_soft_limit.py 500M 2500M
echo 'Finished run .5G/2.5G!'
python3 advanced_test_soft_limit.py 2500M 2500M
echo 'Finished run 2.5G/2.5G!'

for s in 500M 1G 1500M 2G 2500M; do
	python3 advanced_test_soft_limit_alone.py filemicro_seqread.f $s
	echo "Finished run filemicro_seqread.f with ${s}!"

	python3 advanced_test_soft_limit_alone.py filemicro_seqreadone_randreadother.f $s
	echo "Finished run filemicro_seqreadone_randreadother.f with ${s}!"
done
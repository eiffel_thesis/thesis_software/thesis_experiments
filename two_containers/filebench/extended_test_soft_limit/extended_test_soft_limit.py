#! /usr/bin/env python3
# SPDX-License-Identifier: MPL-2.0
import sys
import docker
import os
import threading
import json

"""This python script will start two containers with filebench with the given as
argument soft limits.

It will then run 10 times the FILEBENCH_CONTAINER_A and FILEBENCH_CONTAINER_B in
each container as argument. The output of filebench will be written to a file
and statistics (memory usage in bytes and reads from the disk) will be written
to another file.
"""

# Filebench command for the container A and B.
FILEBENCH_CONTAINER_COMMANDS = ['./filebench -f workloads/filemicro_seqread.f', './filebench -f workloads/filemicro_randread_1G.f']

CHARS = ['A', 'B']

# Each run lasts 60 seconds.
FILEBENCH_DURATION = 60

def thread_func(container, command, out, stats):
	"""Run the command in container and write the output in out.
	This function will be used as a thread.
	:param container: The container in which command will be run. It must already
	been started.
	:type container: docker.models.containers.Container
	:param command: The command to run in container.
	:type command: str
	:param out: An open in write file object where the output of command will be
	written.
	:type out: _io.TextIOWrapper
	:param out: An open in write file object where the stats of the container will
	be written.
	:type out: _io.TextIOWrapper
	"""
	gen = container.stats()

	# Run the benchmark inside the container and write the output to out.
	# exec_run returns a tuple whom second field contains bytes. The decode
	# translate them to a string.
	out.write(container.exec_run(command)[1].decode('utf-8') + '\n')

	statistics = []
	old_read = 0

	for record in gen:
		# Translate the record in string and translate it to python dictionary.
		# Vive la Joz' ! Bon vivant Docteur bon vivant !
		jason = json.loads(record.decode('utf-8'))

		# Create a new dictionary at each iteration so the statistics's cases are
		# differents.
		chicandier = {'usage': jason['memory_stats']['usage']}

		for io in jason['blkio_stats']['io_serviced_recursive']:
			if io['op'] == 'Read':
				# Get the difference between this iteration and the previous one.
				# The value is so the number of read for this second. It can be seen
				# as the bandwith.
				val = io['value'] - old_read

				# Update the old value. Since it begins at 0 the first value will be
				# quite wrong but I do not think it will be a big problem.
				old_read = io['value']

				# Little trick if the key is not already present.
				if 'reads' in chicandier:
					chicandier['reads'] += val
				else:
					chicandier['reads'] = val

		statistics.append(chicandier)

		# Our filebenches last FILEBENCH_DURATION seconds so we just want
		# FILEBENCH_DURATION records since containerd give stats every second.
		if len(statistics) == FILEBENCH_DURATION:
			break

	# Add the statistics to the stat file.
	for i in range(len(statistics)):
		stats.write('%d;%d;%d\n' % (i, statistics[i]['usage'], statistics[i]['reads']))

def main():
	containers = []
	outs = []
	stats = []

	if len(sys.argv) != 3:
		sys.exit("Usage: %s soft_limit0 soft_limit1" % sys.argv[0])

	client = docker.from_env()

	# sys.argv[1:] because sys.argv[0] is the program name.
	for soft_limit in sys.argv[1:]:
		# Launch containers as detached, set their max limits to 3G, their soft
		# limits to given arguments.
		containers.append(client.containers.run('filebench', auto_remove = True, detach = True, mem_limit = '3G', mem_reservation = soft_limit))

	# Open /proc/sys/vm/drop_caches to be able to drop linux page cache.
	drop_cache = open('/proc/sys/vm/drop_caches', 'wb', buffering = 0)

	# Run each benchmark once so their files will be prepared for the next times
	# and they will begin to read files approximately at the same time.
	for i in range(len(containers)):
		containers[i].exec_run(FILEBENCH_CONTAINER_COMMANDS[i])

		# Drop the cache so the run above does not have a consequence on the runs
		# below. It can be a good idea to separate our filebench experiment in two
		# WSL files: one for preparing the run (by creating file) and the other to
		# effectively run the benchmark.
		drop_cache.write(bytearray('3\n', 'utf-8'))

		# sys.argv[i + 1] because sys.argv[0] is the program name and there are
		# len(sys.argv) - 1 containers.
		outs.append(open(os.path.expanduser('~/container_%c_%s_%s.out' % (CHARS[i], containers[i].name, sys.argv[i + 1])), 'w'))
		stats.append(open(os.path.expanduser('~/container_%c_%s_%s.stats' % (CHARS[i], containers[i].name, sys.argv[i + 1])), 'w'))

		stats[i].write('iteration;usage;reads\n')

	# Run each filebench 10 times to compute mean and standard deviation.
	for i in range(10):
		threads = []

		for j in range(len(containers)):
		# Prepare the threads which will run filebench inside the containers.
			threads.append(threading.Thread(target = thread_func, args = (containers[j], FILEBENCH_CONTAINER_COMMANDS[j], outs[j], stats[j])))

		# Launch the threads.
		for t in threads:
			t.start()

		# Wait them.
		for t in threads:
			t.join()

		# Drop the cache between each run so they are independents.
		drop_cache.write(bytearray('3\n', 'utf-8'))

	# Stop and close everything.
	for i in range(len(containers)):
		containers[i].stop()

		outs[i].close()
		stats[i].close()

	drop_cache.close()

if __name__ == "__main__":
    main()
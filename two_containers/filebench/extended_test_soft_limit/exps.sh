#! /usr/bin/env bash
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>

python3 extended_test_soft_limit.py 2700M 300M
echo 'Finished run 2.7G/.3G!'
python3 extended_test_soft_limit.py 2500M 500M
echo 'Finished run 2.5G/.5G!'
python3 extended_test_soft_limit.py 2300M 700M
echo 'Finished run 2.3G/.7G!'
python3 extended_test_soft_limit.py 2100M 900M
echo 'Finished run 2.1G/.9G!'
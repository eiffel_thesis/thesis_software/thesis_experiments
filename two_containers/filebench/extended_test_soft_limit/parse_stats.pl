#! /usr/bin/env perl
# SPDX-License-Identifier: MPL-2.0
use strict;
use warnings;

my $filename;

my %dict;

my $fd_in;
my $fd_out;

$filename = pop or die "Usage: $0 file";

open $fd_in, '<', $filename or die "Can not open '${filename}': $!";
open $fd_out, '>', "${filename}.csv" or die "Can not open '${filename}.csv': $!";

print {$fd_out} "iteration;usage_avg;usage_std;reads_avg;reads_std\n";

%dict = ('usage' => [], 'reads' => []);

# Our stats files contains 60 records from 0 to 59.
foreach my $i (0..59){
	# The array of our keys will be array of array. The array of array will
	# contain all the value for a given 'time'.
	push @{$dict{'usage'}}, [];
	push @{$dict{'reads'}}, [];
}

# Read the file to read the header of the stats file.
<$fd_in>;

while(<$fd_in>){
	# Get the number for ops/s for the IO Summary of each filebench prints.
	if($_ =~ m-(\d+);(\d+);(\d+)-){
		push @{$dict{'usage'}[$1]}, $2;
		push @{$dict{'reads'}[$1]}, $3;
	}
}

foreach my $i (0..59){
	my $usage_avg;
	my $usage_std;

	my $reads_avg;
	my $reads_std;

	my $usage_len;
	my $reads_len;

	$usage_avg = 0;
	$usage_std = 0;

	$reads_avg = 0;
	$reads_std = 0;

	# Single loop mean and standard deviation computing:
	# https://www.strchr.com/standard_deviation_in_one_pass
	foreach my $j (@{$dict{'usage'}[$i]}){
		$usage_avg += $j;

		$usage_std += $j * $j;
	}

	foreach my $j (@{$dict{'reads'}[$i]}){
		$reads_avg += $j;

		$reads_std += $j * $j;
	}

	$usage_len = scalar @{$dict{'usage'}[$i]};
	$usage_avg /= $usage_len;

	$usage_std /= $usage_len;
	$usage_std -= $usage_avg * $usage_avg;
	$usage_std = sqrt $usage_std;

	$reads_len = scalar @{$dict{'reads'}[$i]};
	$reads_avg /= $reads_len;

	$reads_std /= $reads_len;
	$reads_std -= $reads_avg * $reads_avg;
	$reads_std = sqrt $reads_std;

	print {$fd_out} "${i};${usage_avg};${usage_std};${reads_avg};${reads_std}\n";
}

close $fd_in or warn "Problem while closing '${filename}': $!";
close $fd_out or warn "Problem while closing '${filename}.csv': $!";
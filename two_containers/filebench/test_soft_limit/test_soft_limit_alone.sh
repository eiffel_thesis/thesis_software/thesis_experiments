#! /usr/bin/env bash
# SPDX-License-Identifier: MPL-2.0
# This script is basically the same as test_soft_limit.sh but it just runs one
# docker.

if [ $# -ne 2 ]; then
	echo "Usage: ${0} wml_file soft_limit" 1>&2

	exit 1
fi

basenamed=$(basename $1 .f)

# Launch container as detached, set its max limit to 3G, its soft limit to given
# arguments and get its id.
id=$(docker run -d --memory 3G --memory-reservation $2 filebench)

# Echo nothing to the file to emtpy them.
echo > ~/${basenamed}_alone_${2}

# Run the benchmark once so its files will be prepared for the next times.
docker exec $id ./filebench -f workloads/${1}

# Run the benchmark 10 times to compute mean and standard deviation.
for i in {1..10}; do
	docker exec $id ./filebench -f workloads/${1} &>> ~/${basenamed}_alone_${2}
done

docker stop $id
docker rm $id
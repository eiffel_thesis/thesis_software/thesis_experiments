#! /usr/bin/env bash
# SPDX-License-Identifier: MPL-2.0

if [ $# -ne 2 ]; then
	echo "Usage: ${0} soft_limit0 soft_limit1" 1>&2

	exit 1
fi

# Launch containers as detached, set their max limits to 3G, their soft limits
# to given arguments and get their ids.
id0=$(docker run -d --memory 3G --memory-reservation $1 filebench)
id1=$(docker run -d --memory 3G --memory-reservation $2 filebench)

# Echo nothing to the files to emtpy them.
echo > ~/filemicro_seqread_${1}
echo > ~/filemicro_seqread_eachone_${2}

# Run each benchmark once so their files will be prepared for the next times and
# they will begin to read files approximately at the same time.
docker exec $id0 ./filebench -f workloads/filemicro_seqread.f &
docker exec $id1 ./filebench -f workloads/filemicro_seqread_eachone.f &

wait
wait

# Run each benchmark 10 times to compute mean and standard deviation.
for i in {1..10}; do
	docker exec $id0 ./filebench -f workloads/filemicro_seqread.f &>> ~/filemicro_seqread_${1} &
	docker exec $id1 ./filebench -f workloads/filemicro_seqread_eachone.f &>> ~/filemicro_seqread_eachone_${2} &

	wait
	wait
done

docker stop $id0 $id1
docker rm $id0 $id1
#! /usr/bin/env bash
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
# SPDX-License-Identifier: MPL-2.0

for i in alone*/*.out default*/*.out max*/*.out soft*/*.out mechanism*/*.out; do
	perl parse_out.pl $i
	perl parse_out_time.pl $i
done

for i in alone*/*.stats default*/*.stats max*/*.stats soft*/*.stats mechanism*/*.stats; do
	perl parse_stats.pl $i
done

for i in soft*/*_proc_stat mechanism*/*_proc_stat; do
	perl parse_proc_stat.pl $i
done

for i in soft_limit mechanism; do
	cd $i

	# The following perl script need to parse the ouput of trace-cmd report so we
	# firstly need to create it.
	for j in *.dat; do
		echo "$i/$j"
		trace-cmd report -i $j > $j.trace
	done

	cd ..

	perl parse_trace.pl $i/*.trace

	# Parse the 5th files because it is the "middle" of the files.
	perl parse_trace_time.pl $i/*_5*.trace
done

for i in default*/*.color.tar max*/*.color.tar soft*/*.color.tar mechanism*/*.color.tar; do
	# Extract the color.log from the archive and rename it to
	# container_name_#run.color.csv.
	j=$(echo $i | perl -p -e 's-(.*/\w+_\w+_\d)\.color\.tar-$1.color.csv-')
	tar xvf $i --transform "s-color\.log-${j}-"
done
#! /usr/bin/env perl
# SPDX-License-Identifier: MPL-2.0
use strict;
use warnings;
use Statistics::Basic qw(mean stddev);
use Number::Format qw(:subs);


my @KEYS = ('transactions', 'queries', 'latencies');

my $filename;

my $fd_in;
my $fd_out;

my %dict;

$filename = pop or die "Usage: $0 file";

open $fd_in, '<', $filename or die "Can not open '${filename}': $!";
open $fd_out, '>', "${filename}.time.csv" or die "Can not open '${filename}.csv': $!";

print {$fd_out} "time;transactions_avg;transactions_std;queries_avg;queries_std;latencies_avg;latencies_std\n";

foreach my $key (@KEYS){
	$dict{$key} = [];
}

while(<$fd_in>){
	my @matches;

	# Get the transactions, queries and latency for this second.
	if(@matches = ($_ =~ m-\[ (\d+)s.*tps: (\d+\.\d+) qps: (\d+\.\d+).*\(ms,\d+%\): (\d+\.\d+)-g)){
		my $second;

		$second = shift @matches;

		for my $k (0 .. scalar @matches - 1){
			if(! defined $dict{$KEYS[$k]}[$second]){
				$dict{$KEYS[$k]}[$second] = [];
			}

			push @{$dict{$KEYS[$k]}[$second]}, $matches[$k];
		}
	}
}

close $fd_in or warn "Problem while closing '${filename}': $!";

# sysbench counts second from 1 to 1200.
# So we start from 1.
for my $i (1 .. $#{$dict{'transactions'}}){
	my $to_write;

	$to_write = "${i}";

	foreach my $key (@KEYS){
		my $mean;
		my $std_dev;

		# Compute the mean and standard deviation for this key and this second.
		# Statistics::Basic formats also the number.
		# I do not want it so I use the unformat_number of Number::Format which is
		# called by Statistics::Basic.
		# Moreover, formatting use "espace fine insécable" as thousands separator
		# which is not an ascii character but an utf8 one.
		# A simple workaround consists of using binmode(STDOUT, ":utf8") before
		# calling print.
		$mean = unformat_number(mean($dict{$key}[$i]));
		$std_dev = unformat_number(stddev($dict{$key}[$i]));

		$to_write .= ";${mean};${std_dev}";
	}

	printf ${fd_out} "%s\n", $to_write;
}

close $fd_out or warn "Problem while closing '${filename}.time.csv': $!";
#! /usr/bin/env perl
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
# SPDX-License-Identifier: MPL-2.0
use strict;
use warnings;
use Getopt::Long;


my $LOW = 'LOW';
my $MID = 'MID';
my $HIGH = 'HIGH';
my @STATES = ($LOW, $MID, $HIGH);

my $LOW_RATE = 200;
my $HIGH_RATE = 0;

# Set some default values.
my $seed = 42;
my $time = 1080;
my $phase_time = 120;
my @sla = (1800, 1600, 1400, 1400, 1200, 1200, 1000, 800);

my $phase_nr;
my %containers;

my $str;

GetOptions('seed=i' => \$seed, 'time=i' => \$time, 'phase-time=i' => \$time, 'sla=s{1,}' => \@sla) or die "Usage: $0 --seed=random_seed --time=total_time_of_sysbench --phase-time=phase_duration_of_sysbench --sla=sla_of_first_container,...,sla_of_nth_container";

if($time % $phase_time){
	die "Total time must be a multiple of phase duration!";
}

# Initialize random generator with given seed so results can be found again if
# the same seed is given.
srand $seed;

$phase_nr = $time / $phase_time;

foreach my $i (1 .. $phase_nr){
	foreach my $j (0 .. $#sla){
		my $container;
		my $rand;
		my $rate;

		$container = chr(ord('A') + $j);

		# We need to firstly add the container in the containers hash.
		if(!exists ($containers{$container})){
			$containers{$container} = {'rates' => [$LOW_RATE, $sla[$j], $HIGH_RATE], 'result' => ''};
		}

		# We draw a random number between [0, number of states[.
		# The different states are equiprobable.
		# We affect the corresponding rate to $rate.
		$rand = int(rand($#{$containers{$container}{'rates'}} + 1));
		$rate = $containers{$container}{'rates'}[$rand];

		# If the chosen state is MID (middle activity), the associated rate equals:
		# rate = sla +/- 5%.
		if($STATES[$rand] eq $MID){
			if(rand(1) >= .5){
				$rate += $rate / 20;
			}else{
				$rate -= $rate / 20;
			}
		}

		# Append the computed rate to the list of rate.
		$containers{$container}{'result'} .= "$rate,";
	}
}

$str = '';

for(my $i = $phase_time; $i <= $time; $i += $phase_time){
	$str .= "$i,";
}

# Print all the phases time.
chop $str;
print "${str}\n";

# And for each container print its rate over time.
foreach my $key (sort keys %containers){
	chop $containers{$key}{'result'};

	print "${key}: ${containers{$key}{'result'}}\n";
}

=pod

=head1 NAME

name

=head1 SYNOPSIS

	synopsis

=head1 DESCRIPTION

description

=head1 VERSION

version

=head1 SUBROUTINES

subroutines

=head1 DIAGNOSTICS

diagnostics

=head1 CONFIGURATION AND ENVIRONMENT

configuration

=head1 DEPENDENCIES

dependencies

=head1 INCOMPATIBILITIES

incompatibilities

=head1 BUGS AND LIMITATIONS

bugs

=head1 AUTHOR

author

=head1 LICENSE AND COPYRIGHT

licence

=cut
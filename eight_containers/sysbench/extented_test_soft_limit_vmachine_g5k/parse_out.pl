#! /usr/bin/env perl
# SPDX-License-Identifier: MPL-2.0
use strict;
use warnings;


# A run is composed of 9 parts (i.e. 10 * 120 seconds).
my $PARTS = 9;

my $filename;

my $fd_in;
my $fd_out;

my $i;
my $r;

my $queries;
my $latencies;

$filename = pop or die "Usage: $0 file";

open $fd_in, '<', $filename or die "Can not open '${filename}': $!";
open $fd_out, '>', "${filename}.csv" or die "Can not open '${filename}.csv': $!";

$queries = '';
$latencies = '';

print {$fd_out} 'run';

for my $i (0 .. $PARTS - 1){
	$queries .= ";queries_part${i}";
	$latencies .= ";latencies_part${i}";
}
printf {$fd_out} "%s%s\n", $queries, $latencies;

$i = 0;
$r = 0;

$queries = '';
$latencies = '';

while(<$fd_in>){
	# Get the total number of queries done to the database of each sysbench
	# oltp prints.
	if($_ =~ m-queries: +(\d+)-){
		$queries .= ";$1";
	}

	# Get the Xth percentile latency of each sysbench oltp prints.
	if($_ =~ m-\d+th percentile: +(\d+.\d+)-){
		$latencies .= ";$1";

		$i++;

		if($i == $PARTS){
			printf {$fd_out} "%d%s%s\n", $r, $queries, $latencies;

			# Reset everything since we dealt with as many parts which compose a run.
			$i = 0;
			$queries = '';
			$latencies = '';

			# Pass to the next run.
			$r++;
		}
	}
}

close $fd_in or warn "Problem while closing '${filename}': $!";
close $fd_out or warn "Problem while closing '${filename}.csv': $!";
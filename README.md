# Experiments on containers

This repository contains a collection of experiments done on containers to test the existing reclaim mechanism (`max` and `soft` limit) and the solution I developped (MemOpLight).

## Main content

Four directories are of main interest:

- `two_containers/sysbench/extended_test_soft_limit_vmachine_g5k`: It contains results of experiment done with 2 containers testing no mechanism, the `max` and `soft` limit and MemOpLight.
- `two_containers/sysbench/memoplight_percent`: It contains results of experiment done with 2 containers testing different reclaim percent for MemOpLight.
- `two_containers/sysbench/memoplight_period`: It contains results of experiment done with 2 containers testing different reclaim period for MemOpLight.
- `eight_containers/sysbench/extented_test_soft_limit_vmachine_g5k/`: It contains results of experiment done with 8 containers testing no mechanism, the `max` and `soft` limit and MemOpLight.

## Directory content

The above directory contains the following files:

- `*.py`: This files are used to run the experiment for the corresponding mechanism.
They all refer to `common.py`.
- directories: There is one directory per tested mechanism, it contains the results of the experiment for this mechanism as produced by above `*.py` scripts.
- `*.pl`: This files will parse the results of the experiment and mainly produce CSV.
Normally, you should not have to run one of it.
- `parse.sh`: It calls the above `*.pl` scripts.
- `*.ipynb`: Jupyter notebook which presents the results of the experiment.
If you run a new experiment you will need to manually edit them replacing containers files with newly obtained.

## Authors

**Francis Laniel** [<francis.laniel@lip6.fr>](francis.laniel@lip6.fr)

## License

This project is licensed under the MPL 2.0 License.
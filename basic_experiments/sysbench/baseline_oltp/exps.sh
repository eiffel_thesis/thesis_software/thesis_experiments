#! /usr/bin/env bash
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>

for i in 3000M 1500M 1000M 750M 500M 400M; do
	python3 baseline_oltp.py $i
	echo "Finished run oltp for $i!"
done
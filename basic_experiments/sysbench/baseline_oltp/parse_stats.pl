#! /usr/bin/env perl
# SPDX-License-Identifier: MPL-2.0
use strict;
use warnings;

# Experiment lasts 500 seconds so there will 500 records from 0 to 499.
my $NR_RECORDS = 299;

# The keys of our hash.
my @KEYS = ('usage', 'reads');

my $filename;

my %dict;

my $fd_in;
my $fd_out;

$filename = pop or die "Usage: $0 file";

open $fd_in, '<', $filename or die "Can not open '${filename}': $!";
open $fd_out, '>', "${filename}.csv" or die "Can not open '${filename}.csv': $!";

print {$fd_out} "iteration;usage_avg;usage_std;reads_avg;reads_std\n";

%dict = ('usage' => [], 'reads' => []);

# Our stats files contains 60 records from 0 to 59.
foreach my $i (0..$NR_RECORDS){
	# The array of our keys will be array of array. The array of array will
	# contain all the value for a given 'time'.
	push @{$dict{'usage'}}, [];
	push @{$dict{'reads'}}, [];
}

# Read the file to read the header of the stats file.
<$fd_in>;

while(<$fd_in>){
	# Store the statistics in the corresponding array.
	if($_ =~ m-(\d+);(\d+);(\d+)-){
		push @{$dict{'usage'}[$1]}, $2;
		push @{$dict{'reads'}[$1]}, $3;
	}
}

foreach my $i (0..$NR_RECORDS){
	my $to_write;

	$to_write = "${i}";

	# Poor man "generic" loop on statistics (each key is a statistic).
	foreach my $key (@KEYS){
		my $avg;
		my $std;
		my $len;

		$avg = 0;
		$std = 0;

		# Single loop mean and standard deviation computing:
		# https://www.strchr.com/standard_deviation_in_one_pass
		foreach my $j (@{$dict{$key}[$i]}){
			$avg += $j;

			$std += $j * $j;
		}

		$len = scalar @{$dict{$key}[$i]};
		$avg /= $len;

		$std /= $len;
		$std -= $avg * $avg;
		$std = sqrt $std;

		$to_write .= ";${avg};${std}";
	}

	$to_write .= "\n";

	print {$fd_out} $to_write;
}

close $fd_in or warn "Problem while closing '${filename}': $!";
close $fd_out or warn "Problem while closing '${filename}.csv': $!";
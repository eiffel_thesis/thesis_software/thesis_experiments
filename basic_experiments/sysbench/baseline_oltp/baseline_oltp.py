#! /usr/bin/env python3
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
import sys
import docker
import os
import json
import time

"""This python script will start a container with sysbench with the given as
argument memory limit.

It will then run 10 times the wml_file given as argument. The output of
filebench will be written to a file and statistics (memory usage in bytes and
reads from the disk) will be written to another file.
"""

# sysbench last 300 seconds.
DURATION = 300

# The preparation command for sysbench oltp. The database is around 1GB big.
PREPARATION = 'sysbench /usr/share/sysbench/oltp_read_only.lua --table_size=5000000 prepare'

# The run command of sysbench oltp.
RUN = 'sysbench --time=%d /usr/share/sysbench/oltp_read_only.lua --table_size=5000000 run' % DURATION

# The keys added to a temporary dictionary.
KEYS = ['Usage', 'Read']

def main():
	if len(sys.argv) != 2:
		sys.exit("Usage: %s memory_limit" % sys.argv[0])

	client = docker.from_env()

	# Launch container as detached. Its limit will be set once the preparation is
	# done.
	container = client.containers.run('sysbench', auto_remove = True, detach = True, device_read_bps = [{'Path': '/dev/vda', 'Rate': 5 * 1024 ** 2}])

	# Open /proc/sys/vm/drop_caches to be able to drop linux page cache.
	drop_cache = open('/proc/sys/vm/drop_caches', 'wb', buffering = 0)

	# Wait 30 seconds so the sysbench container is ready. Indeed, it can be slow
	# because mysql is long to boot.
	time.sleep(30)

	# Prepare the files
	container.exec_run(PREPARATION)

	# Set the memory limit to given argument. We also need to set the swap limit
	# otherwise there will be exception raised. But since there is no swap on the
	# VM I do not think it is a problem.
	container.update(mem_limit = sys.argv[1], memswap_limit = sys.argv[1])

	# Drop the cache so the preparation above does not have a consequence on the
	# runs below.
	drop_cache.write(bytearray('3\n', 'utf-8'))

	out = open(os.path.expanduser('~/sysbench_oltp_%s.out' % sys.argv[1]), 'w')

	stats = open(os.path.expanduser('~/sysbench_oltp_%s.stats' % sys.argv[1]), 'w')

	stats.write('iteration;usage;reads\n')

	# Run the sysbench 10 times to compute mean and standard deviation.
	for i in range(10):
		gen = container.stats()

		# Run the benchmark inside the container and write the output to out.
		# exec_run returns a tuple whom second field contains bytes. The decode
		# translate them to a string.
		out.write(container.exec_run(RUN)[1].decode('utf-8') + '\n')

		statistics = []
		old_values = {'Read': 0, 'Write': 0}

		for record in gen:
			# Translate the record in string and translate it to python dictionary.
			# Vive la Joz' ! Bon vivant docteur bon vivant !
			jason = json.loads(record.decode('utf-8'))

			# Create a new dictionary at each iteration so the statistics's cases are
			# differents.
			chicandier = {'Usage': jason['memory_stats']['usage']}

			for io in jason['blkio_stats']['io_serviced_recursive']:
				if io['op'] == 'Read':
					# Get the difference between this iteration and the previous one.
					# The value is the number of read or write for this second. It can be
					# seen as the bandwith.
					val = io['value'] - old_values[io['op']]

					# Update the old value. Since it begins at 0 the first value will be
					# quite wrong but I do not think it will be a big problem.
					old_values[io['op']] = io['value']

					# Little trick if the key is not already present.
					# TODO Maybe this if is useless since this code is called each second
					# and chicandier is recreate each second. But it works and I will no
					# loose some time for a little if...
					if io['op'] in chicandier:
						chicandier[io['op']] += val
					else:
						chicandier[io['op']] = val

			statistics.append(chicandier)

			# Our sysbench last 300 seconds so we just want 60 records since
			# containerd give stats every second.
			if len(statistics) == DURATION:
				break

		# Add the statistics to the stat file.
		for i in range(len(statistics)):
			stats.write('%d' % i)

			# Write each statistics in order.
			for key in KEYS:
				stats.write(';%d' % statistics[i][key])

			stats.write('\n')

		# Drop the cache between each run so they are independents.
		drop_cache.write(bytearray('3\n', 'utf-8'))

	# Stop and close everything.
	container.stop()

	out.close()

	stats.close()

	drop_cache.close()

if __name__ == "__main__":
    main()
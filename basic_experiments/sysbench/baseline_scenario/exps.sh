#! /usr/bin/env bash
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>

python3 advanced_test_soft_limit.py 2000M 2000M
echo 'Finished run 2G/2G!'
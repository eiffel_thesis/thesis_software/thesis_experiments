#! /usr/bin/env perl
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>
use strict;
use warnings;

# This array has 3 cases which contains strings.
# There will be one for A files and another for B files.
my @ARRAY = ("'A': [", "'B': [");

my $dir;
my $dir_handle;

my %hash;

my @sorted_keys;
my $len;

my $str;

$dir = shift or die "Usage: $0 csv_dir";

opendir $dir_handle, $dir or die "Problem while openning $dir: $!";

%hash = ();

while(my $file = readdir $dir_handle){
	my @stat;
	my @list;

	my $key;

	if($file !~ /out$/){
		next;
	}

	# Get stats of the file.
	@stat = stat "$dir/$file" or die "Problem while getting stats for $file: $!";

	# Convert epoch of last modified time to human readable time.
	@list = localtime $stat[9];

	# $key is HHMM, there is not colon so we can sort them.
	$key = "$list[2]$list[1]";

	if(exists $hash{$key}){
		push @{$hash{$key}}, $file;
	}elsif(exists $hash{$key - 1}){
		# As detailled above the key is of form HHMM.
		# But it is possible that .out files are not created at the same
		# HHMM even though they are the result of the same experiments.
		# One of the files can be created at HHMM - 1 or HHMM + 1.
		# If there is already a key for HHMM - 1 or HHMM + 1 the file
		# will be added to this key to avoid orphan...
		# I agree this code is complicated by nature as human nature is!
		push @{$hash{$key - 1}}, $file;
	}elsif(exists $hash{$key + 1}){
		push @{$hash{$key + 1}}, $file;
	}else{
		$hash{$key} = [$file];
	}
}

closedir $dir_handle or warn "Problem while closing $dir: $!";

# Sort the keys so they are sorted by increasing HHMM.
@sorted_keys = sort keys %hash;
$len = scalar @sorted_keys - 1;

foreach my $i (0..$len){
	my $l;

	$l = scalar @{$hash{$sorted_keys[$i]}} - 1;

	# Create the string for each container.
	foreach my $j (0..$l){
		my $to_append;

		$to_append = "'$hash{$sorted_keys[$i]}->[$j]'";

		if($i == $len){
			$to_append .= ']';
		}else{
			$to_append .= ', ';
		}

		$ARRAY[$j] .= $to_append;
	}
}

$len = scalar @ARRAY - 1;

$str = "FILES = {";

print ".out.csv files:\n";
foreach my $i (0..$len){
	my $to_append;

	$ARRAY[$i] =~ s/\.out/\.out.csv/g;
	$to_append = $ARRAY[$i];

	if($i != $len){
		$to_append .= ', ';
	}

	$str .= $to_append;
}
$str .= '}';

print "${str}\n";

$str =~ s/\.out/\.stats/g;
print "\n.stats.csv files:\n";
print "${str}\n";
#! /usr/bin/env bash
# SPDX-License-Identifier: MPL-2.0

python3 baseline.py filemicro_seqread.f
echo "Finished run filemicro_seqread.f!"

python3 baseline.py filemicro_randread.f
echo "Finished run filemicro_randread.f!"

python3 baseline.py filemicro_seqread_directio.f
echo "Finished run filemicro_seqread_directio.f!"

python3 baseline.py filemicro_randread_directio.f
echo "Finished run filemicro_randread_directio.f!"
#! /usr/bin/env python3
# SPDX-License-Identifier: MPL-2.0
import sys
import docker
import os
import threading
import json

"""This python script will start a container with filebench..

It will then run 10 times the wml_file given as argument. The output of
filebench will be written to a file and statistics (memory usage in bytes and
reads from the disk) will be written to another file.
"""

def main():
	if len(sys.argv) != 2:
		sys.exit("Usage: %s wml_file" % sys.argv[0])

	client = docker.from_env()

	# Launch container as detached.
	container = client.containers.run('filebench', auto_remove = True, detach = True)

	# Open /proc/sys/vm/drop_caches to be able to drop linux page cache.
	# Write have to be done directly to this file to trigger the cache
	# dropping so they can not be buffered.
	# The drawback of unbuffered I/O is that it is impossible to write
	# string but it is still possible to write bytes (this is the why of
	# 'b' in the mode).
	drop_cache = open('/proc/sys/vm/drop_caches', 'wb', buffering = 0)

	# Run the benchmark once so its files will be prepared for the next times.
	container.exec_run('./filebench -f workloads/%s' % sys.argv[1])

	# Drop the cache so the runs above does not have a consequence on the runs
	# below. It can be a good idea to separate our filebench experiment in two WSL
	# files: one for preparing the run (by creating file) and the other to
	# effectively run the benchmark.
	drop_cache.write(bytearray('3\n', 'utf-8'))

	out = open(os.path.expanduser('~/%s.out' % sys.argv[1]), 'w')

	stats = open(os.path.expanduser('~/%s.stats' % sys.argv[1]), 'w')

	stats.write('iteration;usage;reads\n')

	# Run the filebench 10 times to compute mean and standard deviation.
	for i in range(10):
		gen = container.stats()

		# Run the benchmark inside the container and write the output to out.
		# exec_run returns a tuple whom second field contains bytes. The decode
		# translate them to a string.
		out.write(container.exec_run('./filebench -f workloads/%s' % sys.argv[1])[1].decode('utf-8') + '\n')

		statistics = []
		old_read = 0

		for record in gen:
			# Translate the record in string and translate it to python dictionary.
			# Vive la Joz' ! Bon vivant Docteur bon vivant !
			jason = json.loads(record.decode('utf-8'))

			# Create a new dictionary at each iteration so the statistics's cases are
			# differents.
			chicandier = {'usage': jason['memory_stats']['usage']}

			for io in jason['blkio_stats']['io_serviced_recursive']:
				if io['op'] == 'Read':
					# Get the difference between this iteration and the previous one.
					# The value is so the number of read for this second. It can be seen
					# as the bandwith.
					val = io['value'] - old_read

					# Update the old value. Since it begins at 0 the first value will be
					# quite wrong but I do not think it will be a big problem.
					old_read = io['value']

					# Little trick if the key is not already present.
					if 'reads' in chicandier:
						chicandier['reads'] += val
					else:
						chicandier['reads'] = val

			statistics.append(chicandier)

			# Our filebenches last 30 seconds so we just want 30 records since
			# containerd give stats every second.
			if len(statistics) == 30:
				break

		# Add the statistics to the stat file.
		for i in range(len(statistics)):
			stats.write('%d;%d;%d\n' % (i, statistics[i]['usage'], \
				statistics[i]['reads']))

		# Drop the cache between each run so they are independents.
		drop_cache.write(bytearray('3\n', 'utf-8'))

	# Stop and close everything.
	container.stop()

	out.close()

	stats.close()

	drop_cache.close()

if __name__ == "__main__":
    main()
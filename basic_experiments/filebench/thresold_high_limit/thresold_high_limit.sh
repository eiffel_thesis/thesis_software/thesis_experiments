#! /usr/bin/env bash
# SPDX-License-Identifier: MPL-2.0
# The sizes we want to test. They are arbirtrary/dichotomy set.
sizes='850M 900M 1000M 1025M 1037M 1040M 1043M 1050M 1100M'

# Launch filebench container as detached and get its id.
id=$(docker run -d filebench)

for s in $sizes; do
	echo "Size is: $s."
	# Change the high limit.
	echo $s > /sys/fs/cgroup/memory/docker/$id/memory.high_limit_in_bytes

	# And run the container.
	docker exec $id ./filebench -f workloads/filemicro_seqread.f
done

docker stop $id
docker rm $id
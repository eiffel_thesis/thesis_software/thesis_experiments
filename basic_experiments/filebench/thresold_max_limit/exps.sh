#! /usr/bin/env bash
# SPDX-License-Identifier: MPL-2.0
# Copyright (c) 2019 Francis Laniel <francis.laniel@lip6.fr>

for s in 500M 750M 1000M 1250M 1500M 1750M 2000M 2250M 2500M; do
	python3 thresold_max_limit.py filemicro_seqread.f $s
	echo "Finished run filemicro_seqread.f with ${s}!"

	python3 thresold_max_limit.py filemicro_randread.f $s
	echo "Finished run filemicro_randread.f with ${s}!"
done